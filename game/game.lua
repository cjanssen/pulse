game = game or {}

function game.load()
	VideoModes.init()
	createBigBatch()
	game.initfont()
	Options.load()
	VideoModes.reloadScreen()
	Sounds.init()
	Titles.init()
	game.initvalues()
	player.init()
	Plankton.init()
	Circles.init()
	Friends.init()
	Connectors.init()
	game.centerInPlayer()
	Sounds.start()
	RhythmDisplay.init()
	PounceControl.init()
end

function game.restartGame()
	game.newgamevalues()
	Titles.restart()
	player.restart()
	Circles.restart()
	Friends.restart()
	Connectors.restart()
	RhythmDisplay.reset()
	PounceControl.reset()
	game.centerInPlayer()
	Sounds.start()
end

function game.quitting()
	Options.save()
end

-- game modes
-- 1 . title
-- 2 . ingame
-- 3 . endtitle
-- 4 . options
function game.initvalues()
	love.window.setIcon(love.graphics.newImage("img/icon.png"):getData())
	love.mouse.setVisible(false)

	local baseSize = 15000
	game.worldSize = { baseSize, baseSize * 3 * 0.5 / math.sqrt(3) }
	game.layerFactors = { 1, 0.5, 0.25 }
	game.fps = 0
	game.newgamevalues()
end

function game.newgamevalues()
	game.friendgoal = 8
	game.restart = false
	game.setMode(1)
	game.layerCenters = { { 640, 480 }, { 640, 480 }, { 640, 480 } }
	game.screenSpeed = 4
	game.screenDrag = 0.95
	game.screenVel = {0,0}

	game.updateScaling(0, true)
	game.quadPlankton = true

	game.zoomEnergy = 1
	game.zoomThreshold = 0.2
	game.zoomLevel = game.minZoom
	game.zoom = game.zoomLevel

	game.anchorPoint = nil
	game.followPoint = player.pos


	game.showingHelp = true
end

function game.testingValues()
	game.hasFriends = true
	game.showFPS = false
end

function game.initfont()
	game.font = love.graphics.newFont("fnt/Mathlete-Bulky.otf", 84)
	love.graphics.setFont(game.font)
end

function love.update(dt)
	if dt > 0.5 then return end -- avoid jumps below 2fps
	game.updateScaling(dt)

	Circles.prepareShader()
	Titles.update(dt)
	Options.update(dt)	
	player.update(dt)
	Plankton.update(dt)
	if game.mode ~= 1 then
		RhythmDisplay.update(dt)
		Circles.update(dt)
		Connectors.update(dt)
		Friends.update(dt)
		PounceControl.update(dt)
	end
	game.screenupdate(dt)

	if game.restart then
		game.restartGame()
	end

	if game.showFPS then
		if dt > 0 then
			game.fps = 0.9 * game.fps + 0.1 / dt
		else
			game.fps = 0.9 * game.fps
		end
	end
end

function game.screenupdate(dt)
	if game.anchorPoint then
		game.followPoint = game.anchorPoint
	else
		game.followPoint = player.pos
	end

	game.parallax(dt, game.layerCenters[1], 1)
	game.layerCenters[2][1] = game.layerCenters[1][1]/2
	game.layerCenters[2][2] = game.layerCenters[1][2]/2
	game.layerCenters[3][1] = game.layerCenters[1][1]/4
	game.layerCenters[3][2] = game.layerCenters[1][2]/4
end

function game.parallax(dt, layer, factor)
	local dx = game.followPoint[1] - layer[1]
	local dy = game.followPoint[2] - layer[2]

	-- wrap around
	if dx > game.worldSize[1] / 2 then
		layer[1] = layer[1] + game.worldSize[1]
		dx = game.followPoint[1] - layer[1]
	elseif dx < -game.worldSize[1]/2 then
		layer[1] = layer[1] - game.worldSize[1]
		dx = game.followPoint[1] - layer[1]
	end
	if dy > game.worldSize[2] / 2 then
		layer[2] = layer[2] + game.worldSize[2]
		dy = game.followPoint[2] - layer[2]
	elseif dy < -game.worldSize[2]/2 then
		layer[2] = layer[2] - game.worldSize[2]
		dy = game.followPoint[2] - layer[2]
	end

	local p = math.pow(game.screenDrag,dt*60)

	game.screenVel = {
		game.screenVel[1] * p + dx * game.screenSpeed * dt,
		game.screenVel[2] * p + dy * game.screenSpeed * dt
	}

	layer[1] = layer[1] + game.screenVel[1] * dt
	layer[2] = layer[2] + game.screenVel[2] * dt
end

function game.centerInPlayer()
	game.followPoint = player.pos
	game.layerCenters[1][1] = game.followPoint[1]
	game.layerCenters[1][2] = game.followPoint[2]
	game.layerCenters[2][1] = game.layerCenters[1][1]
	game.layerCenters[2][2] = game.layerCenters[1][2]
	game.layerCenters[3][1] = game.layerCenters[1][1]
	game.layerCenters[3][2] = game.layerCenters[1][2]
end

function game.checkAnchor(pos)
	if not game.anchorPoint then
		local corrected = {wrapVec(player.pos, pos)}
		game.anchorPoint = {
			player.pos[1] + corrected[1]*0.5,
			player.pos[2] + corrected[2]*0.5,
		}
		game.zoomIn = true
		PounceControl.enable()
	end
end

function game.removeAnchor()
	game.anchorPoint = nil
	game.zoomIn = false
	PounceControl.disable()
end

function game.updateScaling(dt, insta)
	if game.mode ~= 4 then 
		game.minZoom = 0.55
		game.maxZoom = 0.78
		game.zoomLevel = game.zoomIn and game.maxZoom or game.minZoom

		if insta then
			game.zoom = game.zoomLevel
		else	
			game.zoom = linearApprox(dt, game.zoom, game.zoomLevel, 0.1)
		end
	end

	VideoModes.scale = {VideoModes.zoom[1] * game.zoom, VideoModes.zoom[2] * game.zoom}
	love.graphics.setLineWidth(math.max(1, 1/VideoModes.scale[1], 1/VideoModes.scale[2]))
	game.screenWidth = Options.width/VideoModes.scale[1]
	game.screenHeight = Options.height/VideoModes.scale[2]
end

function love.draw()
	love.graphics.push()
	love.graphics.scale(VideoModes.scale[1], VideoModes.scale[2])
	-- love.graphics.setBackgroundColor(128,192,255,32)
	love.graphics.setBackgroundColor(15,23,31)
	love.graphics.setColor(255,255,255)

	Batch:clear()
	
	Plankton.draw()
	if game.mode ~= 1 then
		Circles.draw()
		Connectors.draw()
		Friends.draw()
	end
	player.draw()

	Plankton.drawBatches()
	love.graphics.draw(Batch)

	if game.mode ~= 1 then
		RhythmDisplay.draw()
	end

	if game.showFPS then
		love.graphics.setColor(255,255,255)
		local yy = 20
		if game.showPlayerCoords then
			yy = yy + 20
		end
		love.graphics.push()
		love.graphics.scale(0.5/game.zoom,0.5/game.zoom)
		love.graphics.print(math.floor(game.fps),yy,0)
		love.graphics.pop()
	end
	
	Options.draw()
	Titles.draw()

	love.graphics.pop()
end

function game.setMode(newMode)
	if game.mode == 1 and newMode == 2 then
		Friends.avoidPlayer()
	elseif newMode == 3 then
		Sounds.tempMuteSfx()
	end
	game.mode = newMode
end


function love.keypressed( key )
	if key == "escape" then
		if game.mode == 4 then
			Options.toggle()
		else
			love.event.push("quit")
			return
		end
	end

	if key == "f1" then
		Options.toggle()
	end

end

function love.mousepressed(x,y, button)
	-- ignore mousewheel events
	if button ~= "l" and button ~= "r" then return end

	if game.mode == 4 then
		Options.registerClick()
		return
	end
		
	PounceControl.pressed(x,y)
	
end

function love.mousereleased(x,y)
	-- ignore mousewheel events
	if button ~= "l" and button ~= "r" then return end

	Options.unregisterClick()
end
