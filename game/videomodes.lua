VideoModes = VideoModes or {}

function VideoModes.init()

	VideoModes.scale = {1, 1}
	VideoModes.zoom = {1, 1}
	game.zoom = 1
	VideoModes.fList = {} -- love.window.getFullscreenModes( )[1] }
	

   local w, h = love.window.getDesktopDimensions()
   table.insert(VideoModes.fList, { width = w, height = h } )

	VideoModes.wList = {
	  { width = 1920, height = 1440 },	
	  { width = 1920, height = 1080 },
	  { width = 1680, height = 1050 },
	  { width = 1280, height = 1024 },
	  { width = 1280, height = 960 },
	  { width = 1280, height = 720 },
	  { width = 1024, height = 768 },
	  { width = 800, height = 600 },
	  { width = 640, height = 480 } }

	for i,v in ipairs(VideoModes.fList) do	
		table.insert(VideoModes.wList, v)
	end

	local function removeDupls(list)
		table.sort(list, function(a, b) return a.width*a.height > b.width*b.height end)
		for i=#list,1,-1 do
			if list[i].width < 640 or 
				list[i+1] and 
				list[i].width == list[i+1].width and 
				list[i].height == list[i+1].height then
				table.remove(list, i)
			end
		end 
	end

	removeDupls(VideoModes.fList)
	removeDupls(VideoModes.wList)

end
 
function VideoModes.getScreenDefaultOption(fullscreen)
	local fsm = love.window.getFullscreenModes()

	if #fsm > 0 then
		return fsm[1]
	end

	return nil
end

function VideoModes.reloadScreen()
	if Options.width == 0 then
		return
	end

	if game.screenWidth ~= Options.width or game.screenHeight ~= Options.height or game.fullscreen ~= Options.fullscreen then
		Options.unregisterClick()

		local success = love.window.setMode( Options.width, Options.height, { fullscreen = Options.fullscreen, fullscreentype = "desktop" } )
		if success then
			game.screenWidth = love.graphics.getWidth()
			game.screenHeight = love.graphics.getHeight()
			game.fullscreen = love.window.getFullscreen()
			Options.width = game.screenWidth
			Options.height = game.screenHeight
			Options.fullscreen = game.fullscreen
		end
	end

	-- post-step
	VideoModes.zoom = {game.screenWidth / 1024, game.screenWidth / 1024}
	game.updateScaling(0, true)
	Options.initUI()
end


