TileCoords = {
	big_cell_core_1 = { 0, 0, 308, 311},
	big_cell_core_2 = { 308, 0, 308, 311},
	big_cell_core_3 = { 616, 0, 308, 311},
	big_cell_slime_1 = { 924, 0, 449, 453},
	core_1 = { 1373, 0, 143, 143},
	core_2 = { 1516, 0, 143, 143},
	core_3 = { 0, 453, 143, 143},
	big_cell_slime_2 = { 143, 453, 449, 453},
	plankton_500_1 = { 592, 453, 250, 250},
	plankton_500_2 = { 842, 453, 250, 250},
	plankton_500_3 = { 1092, 453, 250, 250},
	plankton_500_4 = { 1342, 453, 250, 250},
	big_cell_slime_3 = { 0, 906, 449, 453},
	plankton_500_5 = { 449, 906, 250, 250},
	player_cell_4 = { 699, 906, 449, 449},
	round_1 = { 1148, 906, 200, 200},
	round_2 = { 1348, 906, 200, 200},
	round_3 = { 1548, 906, 200, 200},
	sharp_1 = { 0, 1359, 200, 200},
	sharp_2 = { 200, 1359, 200, 200},
	big_cell_slime_4 = { 400, 1359, 449, 453},
	sharp_3 = { 849, 1359, 200, 200},
	soft_1 = { 1049, 1359, 200, 200},
	soft_2 = { 1249, 1359, 200, 200},
	soft_3 = { 0, 1812, 200, 200},
	title = { 200, 1812, 268, 133},
	wheel = { 468, 1812, 142, 142},
	backarrow = { 610, 1812, 128, 96},
	triangle = { 738, 1812, 36, 38}
}

local batchimg = false

function createBigBatch()
	Batch = getNewBatch()
end

function getNewBatch()
	batchimg = batchimg or love.graphics.newImage("img/tileset.png")
	local b = love.graphics.newSpriteBatch(batchimg, 2000, "static")
	return b
end

function getQuad(quadname)
	local c = TileCoords[quadname]
	if not c then return nil end
	return love.graphics.newQuad(c[1],c[2],c[3],c[4],Batch:getImage():getWidth(),Batch:getImage():getHeight())
end