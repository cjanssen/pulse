
function updateMove( entity, dt )
	-- drag and acc
	local p = math.pow(entity.drag, dt*60)
	entity.vel[1] = entity.vel[1] * p + entity.acc[1] * dt
	entity.vel[2] = entity.vel[2] * p + entity.acc[2] * dt
	
	entity.pos[1] = (entity.pos[1] + entity.vel[1] * dt) %game.worldSize[1]
	entity.pos[2] = (entity.pos[2] + entity.vel[2] * dt) %game.worldSize[2]

	-- rotation
	entity.angle = (entity.angle + entity.rotationSpeed * dt)%360
end

function updateMoveFixedVel( entity, dt )
	-- drag
	entity.pos[1] = entity.pos[1] + entity.vel[1] * dt
	entity.pos[2] = entity.pos[2] + entity.vel[2] * dt
	
	entity.pos[1] = (entity.pos[1] + entity.vel[1] * dt) %game.worldSize[1]
	entity.pos[2] = (entity.pos[2] + entity.vel[2] * dt) %game.worldSize[2]

	-- rotation
	entity.angle = (entity.angle + entity.rotationSpeed * dt)%360
end

function mapCoords( entity )
	local layer = entity.layer
	entity.screenPos[1] = (entity.pos[1]*game.layerFactors[layer] - game.layerCenters[layer][1] + game.screenWidth) % (game.worldSize[1]*game.layerFactors[layer]) - game.screenWidth*0.5
	entity.screenPos[2] = (entity.pos[2]*game.layerFactors[layer] - game.layerCenters[layer][2] + game.screenHeight) % (game.worldSize[2]*game.layerFactors[layer]) - game.screenHeight*0.5
end

function updateEntityBeat( entity, dt )
	entity.pulse.timer = entity.pulse.timer + dt
	if entity.heartRate > 10 and entity.pulse.timer >= 1200 / entity.heartRate then
		entity.pulse.timer = 0
	end

	local x = entity.pulse.timer * 10
	local val = x * math.exp(-x)
	entity.pulse.amount = entity.pulse.min + val * entity.pulse.amp
	
end
