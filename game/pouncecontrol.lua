
PounceControl = PounceControl or {}
local pC = nil


function PounceControl.init()
    PounceControl.reset()
end

function PounceControl.reset()
    PounceControl.data = {
        active = false,
        silenceDelay = player.heartPeriod * 1.5,
        silenceTimer = 0,
        sequenceLen = 2,
        sequenceTimes = {},
        isPouncing = false,
    }
    pC = PounceControl.data

end

function PounceControl.enable()
    if not PounceControl.data.active then
        PounceControl.data.active = true
        PounceControl.prepare()
        pC.silenceTimer = 0
        pC.isPouncing = false
    end
end

function PounceControl.disable()
    if PounceControl.data.active then
        PounceControl.data.active = false
        RhythmDisplay.hide()
    end
end

function PounceControl.pressed(x,y)
    if not pC.active then
        return false
    end

    pC.pressedPos = {x,y}

end


function PounceControl.createSequence()
    local t = love.timer.getTime()
    pC.sequenceTimes = {}
    for i=1,pC.sequenceLen-1 do
        pC.sequenceTimes[i] = t + i * player.heartPeriod
    end
end

function PounceControl.beat()
    if #pC.sequenceTimes == 0 then
        PounceControl.createSequence()
        PounceControl.firstFire()
    else
        local desiredTime = table.remove(pC.sequenceTimes, 1)
        local tdiff = math.abs(love.timer.getTime() - desiredTime)
        if tdiff <= pC.tolerance then
            if #pC.sequenceTimes > 0 then
                PounceControl.fire()
            else
                PounceControl.finalFire()
            end
        else
            if #pC.sequenceTimes == pC.sequenceLen - 2 then
                -- restart
                PounceControl.createSequence()
                PounceControl.firstFire()
            else
                PounceControl.misfire()
            end
        end
    end

end

function PounceControl.prepare()
    pC.sequenceTimes = {}
    if Friends.firstPulse then
        PounceControl.data.sequenceLen = math.max(2,#player.followers+1)
    else
        PounceControl.data.sequenceLen = 5
    end
    RhythmDisplay.preparePounce(PounceControl.data.sequenceLen)
    RhythmDisplay.show()
    Circles.resetAlpha()
end

function PounceControl.firstFire()
    player.fire(0)
    RhythmDisplay.startPounce(PounceControl.data.sequenceLen)
    Circles.pounceStart(player)  
    pC.isPouncing = true 

    if Circles.params.globalAlpha < 0.05 then
        if Titles.helpPhase >= 4 then
            Titles.helpPhase = 0
            Titles.helpOpacity = 0
        end
        PounceControl.scheduleReset()
    end
end

function PounceControl.fire()
    player.fire(PounceControl.getFireStep())
    RhythmDisplay.continuePounce()
    Circles.pounceContinue(player)    
end

function PounceControl.finalFire()
    pC.silenceTimer = pC.silenceDelay
    player.fire(PounceControl.getFireStep())
    player.finalFire()
    RhythmDisplay.finishPounce()
    Circles.pounceFinish(player)
    pC.sequenceTimes = {}
    pC.isPouncing = false
end

function PounceControl.misfire()
    pC.silenceTimer = pC.silenceDelay
    player.misfire()
    RhythmDisplay.missedPounce()
    pC.sequenceTimes = {}
    pC.isPouncing = false
end

function PounceControl.scheduleReset()
    pC.silenceTimer = pC.silenceDelay
    pC.sequenceTimes = {}
    pC.isPouncing = false
end

function PounceControl.checkDelayed()
    if #pC.sequenceTimes == 0 then return end
    local t = love.timer.getTime()
    if t >= pC.sequenceTimes[1] + player.heartPeriod + pC.tolerance then
        -- delayed!

        if Circles.params.globalAlpha < 0.15 then
            if Titles.helpPhase >= 4 then
                Titles.helpPhase = 0
                Titles.helpOpacity = 0
            end
            PounceControl.scheduleReset()
        else
            PounceControl.misfire()
        end
    end
end

function PounceControl.getFireStep()
    return pC.sequenceLen - #pC.sequenceTimes - 1
end

function PounceControl.ready()
    if not pC.active then return false end
    if not pC.pressedPos then return false end
    if pC.silenceTimer > 0 then return false end
    if game.showingHelp then return false end
    if game.mode == 4 then return false end
    if Friends.annoyedCount > 0 then return false end
    if Titles.helpPhase < 4 then return false end

    local dx,dy = love.mouse.getX() - pC.pressedPos[1], love.mouse.getY() - pC.pressedPos[2]
    if dx*dx+dy*dy < 1000 then
        return true
    end
    return false
end

function PounceControl.update(dt)
    if not pC.active then return end

    pC.tolerance = Circles.params.toleranceTime * 0.5

    if Titles.helpPhase < 4 then 
        pC.silenceTimer = 0.1
    end

    if pC.silenceTimer > 0 then
        pC.silenceTimer = pC.silenceTimer - dt
        pC.pressedPos = nil
        if pC.silenceTimer <= 0 then
            PounceControl.prepare()
        end
    end

    -- consume pounce
    if PounceControl.ready() then
        PounceControl.beat()
        pC.pressedPos = nil
    end

    PounceControl.checkDelayed()
end


-- does not have "draw", it's only a controller
-- function PounceControl.draw()
-- end
