player = player or {}

function player.init()
	player.pos = { 4200, 4200 }
	player.layer = 1
	player.screenPos = { 640, 480 }
	player.vel = { 0, 0 }
	player.acc = { 0, 0 }
	player.af = 4.5 -- af is acceleration factor
	player.drag = 0.97
	player.heartRate = 1000
	player.heartPeriod = 1200 / player.heartRate
	player.followers = {}
	player.watchers = {}

	player.pulse = {
		timer = 0,
		min = 0.4,
		amp = 0.2,
		amount = 0.2,
		shrink = 0.85,
	}

	player.rotationSpeed = 0
	player.maxRotationSpeed = 120 -- degrees per second
	player.rotationAcc = 0
	player.angle = 0
	player.hookAngle = 0
	player.hookDist = 95

	player.fails = 0
	player.failLimit = 1
	player.mistrust = 0

	player.colors = {
		time = 0,
		period = player.heartPeriod * 1.15,
		current = {120, 0, 255},
		modifier = {120, 0, 255},
		base = {120, 0, 255},

		positive = {120, 48, 255},
		negative = {300, 0, 192},
		affected = {300, 48, 255},
		charge = {120, 64, 255}
    }

    player.detacher  = {
    	time = 0,
    	delay = 4
	}

	player.playerNote = Sounds.getFriendNote()
	player.secondNote = Sounds.getFriendNote()
	player.getStartingNotes()
	
    player.img = getQuad("player_cell_4")
    local ix,iy,iw,ih = player.img:getViewport()
    player.halfsize = {iw*0.5,ih*0.5}
end

function player.restart()
	player.pos = { 4200, 4200 }
	player.layer = 1
	player.screenPos = { 640, 480 }
	player.vel = { 0, 0 }
	player.acc = { 0, 0 }
	player.followers = {}
	player.watchers = {}

	player.pulse.timer = 0
	player.rotationSpeed = 0
	player.angle = 0
	player.hookAngle = 0

	player.fails = 0
	player.mistrust = 0

	player.colors.time = 0
	player.colors.current = {120,0,255}

    player.detacher.time = 0
    player.getStartingNotes()
end

function player.getStartingNotes()
	player.startingNotes = {}
	for i=1,5 do
		player.startingNotes[i] = Sounds.getFriendNote()
	end
end

function player.update(dt)
	updateEntityBeat(player, dt)
	player.updateMessage( dt )
	player.updateAffectors( dt )
	player.updateHook( dt )
	player.manageMove( dt )
	updateMove( player, dt )
	mapCoords( player )

	-- player.finger.opacity = decreaseExponential(dt, player.finger.opacity,0.9)
end

function player.manageMove( dt )
	-- accelerate in the direction of the mouse
	local mouseX, mouseY

	if game.mode == 4 then
		return
	end

	if game.mode == 2 then
		mouseX, mouseY = love.mouse.getX() / VideoModes.scale[1], love.mouse.getY() / VideoModes.scale[2]

	elseif game.mode == 1 then
		mouseX = love.graphics.getWidth() / VideoModes.scale[1] * 0.95
		mouseY = love.graphics.getHeight() / VideoModes.scale[2] * 0.90
	else
		mouseX = love.graphics.getWidth() / VideoModes.scale[1] * 0.5
		mouseY = love.graphics.getHeight() / VideoModes.scale[2] * 0.5
	end

	local dx = mouseX - player.screenPos[1]
	local dy = mouseY - player.screenPos[2]

	player.acc[1] = dx * player.af
	player.acc[2] = dy * player.af

	player.updateRotationSpeed( dt )

end

function player.updateRotationSpeed( dt )
	local accAngle = getAngle({0, 0}, player.acc)
	local dirAngle = getAngle({0, 0}, player.vel)
	if player.acc[1] == 0 and player.acc[2] == 0 then
		accAngle = dirAngle
	end
	player.rotationAcc = player.rotationAcc * 0.99 + (accAngle - dirAngle) * 2 * dt
	player.rotationSpeed = player.rotationSpeed * 0.99 + player.rotationAcc * dt


	if math.abs(player.rotationSpeed) > player.maxRotationSpeed then
		player.rotationSpeed = math.sign(player.rotationSpeed) * player.maxRotationSpeed
	end
end

function player.updateAffectors( dt )
	if player.colors.time <= 0 then
		player.colors.current = player.colors.base
	else
		player.colors.time = player.colors.time - dt
		player.colors.current = player.colors.modifier
	end

	local ampDest = 0.2
	local minDest = 0.4
	player.pulse.amp = linearApprox(dt, player.pulse.amp, ampDest, 2)
	player.pulse.min = linearApprox(dt, player.pulse.min, minDest, 2)

	if player.colors.time > 0 then
		local frac = (player.colors.period - player.colors.time) / player.colors.period
		frac = math.pow(frac, 2.88)
		local sizeMult = frac * (1-player.pulse.shrink) * frac + player.pulse.shrink

		player.pulse.amp = player.pulse.amp * sizeMult
		player.pulse.min = player.pulse.min * sizeMult
	end
end

function player.updateHook( dt )
	player.hookAngle = player.hookAngle + math.sign(player.rotationSpeed) * 10 * dt
	if player.hookAngle > 360 then
		player.hookAngle = player.hookAngle - 360
	elseif player.hookAngle < 0 then
		player.hookAngle = player.hookAngle + 360
	end

	if game.zoomEnergy < 0.7 or #player.followers == 0 then
		-- no detach
		player.detacher.time = 0
	else
		player.detacher.time = player.detacher.time + dt
		if player.detacher.time >= player.detacher.delay then
			player.detacher.delay = 3 + math.random()*12
			player.detacher.time = 0
			local hkcnt = {}

			for i=1,#player.followers do
				local friend = player.followers[i]
				local dx = player.pos[1] - friend.pos[1] 
				local dy = player.pos[2] - friend.pos[2]
				local ang = angleDiff(getOrientation({dx,dy}),getOrientation(player.vel))
				if friend.hook.hooked and math.abs(ang) < 30  then
					table.insert(hkcnt, friend)
				end
			end
			if #hkcnt > 0 then
				local friend = hkcnt[math.random(#hkcnt)] 
				friend.hook.hooked = false
				Connectors.newLink(player,friend, true)
				friend.velmul = 0.8
			end
		end
	end

end

function player.draw()
	-- color
	Batch:setColor(hsv2rgb(player.colors.current[1], player.colors.current[2], player.colors.current[3], 240 * Titles.characterOpacity))

	Batch:add(player.img, 
		player.screenPos[1], player.screenPos[2], 
		player.angle * math.pi / 180, 
		player.pulse.amount * 2,
	 	player.pulse.amount * 2,
 		player.halfsize[1], player.halfsize[2])
end

function player.updateMessage( dt )
	if player.pulse.timer == 0 then
		if game.mode == 1 or game.mode == 2 then
			Sounds.playHeartbeat()
		end
	end
end

function player.fire(step)
	Friends.affectWatchers()
	if #player.followers == 0 then
		if Friends.firstPulse then
			Sounds.playNoteVolume(player.playerNote, Circles.params.globalAlpha)
		else
			Sounds.playNoteVolume(player.startingNotes[step+1], Circles.params.globalAlpha)
		end
	elseif step < #player.followers then
		Sounds.playNoteVolume(player.followers[step+1].notes.pulse, Circles.params.globalAlpha)
		Friends.startEcho(player.followers[step+1], player.followers[step+1].notes.pulse, Circles.params.globalAlpha)
	end
end

function player.finalFire()
	Sounds.playNote(player.secondNote)
	Friends.finalPounce()
end

function player.misfire()
	-- on every miss, indicate it
	player.colors.modifier = player.colors.negative
	player.colors.time = player.colors.period
	Sounds.playNote(Sounds.failNote)

	-- but friends only react every so fails
	player.fails = player.fails + 1
	if player.fails >= player.failLimit then
		player.fails = 0	

		Friends.checkMiss()
	end
end

