
function include( filename )
	love.filesystem.load( filename )()
end


function loadDependencies()
	include("tilecoords.lua")
	include("utils.lua")
	include("game.lua")
	include("player.lua")
	include("entity.lua")
	include("plankton.lua")
	include("circles.lua")
	include("friends.lua")
	include("sounds.lua")
	include("titles.lua")
	include("options.lua")
	include("videomodes.lua")
	include("quadtrees.lua")
	include("connector.lua")
	include("rhythmdisplay.lua")
	include("pouncecontrol.lua")
end

function love.load()
	loadDependencies()
	game.testingValues()
	game.load()
end

function love.quit()
	game.quitting()
end
