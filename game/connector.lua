Connectors = Connectors or {}

function Connectors.init()
	Connectors.list = {}

	Connectors.halfwidth = 30
	local ch = Connectors.halfwidth
	local m = Connectors.halfwidth*2
	local imgd = love.image.newImageData(m,m)
	for x=0,m-1 do
		for y=0,m-1 do
			local dx,dy = (ch-x)/ch,(ch-y)/ch
			local r = dx*dx+dy*dy
			local c = math.max(0, 1-r)
			imgd:setPixel(x,y,255,255,255,math.pow(c,2)*255)
		end
	end

	Connectors.img = love.graphics.newImage(imgd)
	Connectors.batch = love.graphics.newSpriteBatch(Connectors.img,1000,"static")
	Connectors.quad = love.graphics.newQuad(0,0,m,m,m,m)
end

function Connectors.restart()
	Connectors.list = {}
end

local function makeline(link)
	local line = link.line
	line.linelen = 0

	local x,y,r,c = 0,0,0,0
	local xfrom,yfrom = link.ef.screenPos[1], link.ef.screenPos[2]
	local distx = link.et.screenPos[1]- link.ef.screenPos[1]
	local disty = link.et.screenPos[2]- link.ef.screenPos[2]
	local dist = math.sqrt(distx*distx+disty*disty)
	local incx,incy = distx/dist,disty/dist
	local step = 8/dist
	local yamp = 0.2 * dist + 40
	local dv = math.sin(link.divphase) * 0.5 + 2
	for jj=0,link.jlim,step do
		local xval = jj * dist
		local a = math.sin(jj * math.pi)
		local ns = love.math.noise(jj*dv+1000+link.x0) * 2 - 1
		local yval = a * ns * yamp
		r = love.math.noise(jj*3+link.x0*2+500) * 1.2 + 0.5	
		c = love.math.noise(jj*4.5+link.x0*0.75) * 0.9 + 0.1
		local cfac = ((1.0-jj) * 0.9 + 0.1)
		c = math.pow(c,1.5) * 64 * cfac

		x = xfrom + xval * incx - yval * incy 
		y = yfrom + xval * incy + yval * incx

		line.linelen = line.linelen + 1
		line[line.linelen] = {x = x,y = y,r = r,c = c}
	end
end

function Connectors.newLink(fromEntity,toEntity, insta)
	local link = {
		jlim = insta and 1 or 0,
		jspd = 0.23,
		x0 = 0,
		xspd = 0.25,
		divphase = 0,
		divfreq = 0.2,
		ef = fromEntity,
		et = toEntity,
		line = { linelen = 0 },
	}

	for i=1,#Connectors.list do
		if Connectors.list[i].et == toEntity and Connectors.list[i].ef == fromEntity then
			Connectors.list[i] = link
			return
		end
	end

	table.insert(Connectors.list, link)
end

function Connectors.removeLink(fromEntity,toEntity)
	for i=1,#Connectors.list do
		if Connectors.list[i].et == toEntity and Connectors.list[i].ef == fromEntity then
			table.remove(Connectors.list, i)
			return
		end
	end
end

function Connectors.update(dt)
	for i=1,#Connectors.list do
		local link = Connectors.list[i]
		link.x0 = link.x0 - dt * link.xspd
		link.divphase = (link.divphase + dt * link.divfreq) % (2*math.pi)

		if link.jlim < 1 then
			link.jlim = math.min(1, link.jlim + dt * link.jspd)
		end

		makeline(link)
	end
end

function Connectors.draw()
	if #Connectors.list == 0 then return end
	
	local ch = Connectors.halfwidth
	Connectors.batch:clear()
	for i=1,#Connectors.list do
		for j=1,Connectors.list[i].line.linelen do
			local circ = Connectors.list[i].line[j]
			if circ then
				Connectors.batch:setColor(240,248,255,circ.c * Titles.characterOpacity)
				Connectors.batch:add(Connectors.quad,circ.x,circ.y,0,circ.r,circ.r,ch,ch)
			end
		end
	end

	love.graphics.draw(Connectors.batch)
end
