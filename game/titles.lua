Titles = Titles or {}

function Titles.init()
	Titles.titleBanner = getQuad("title")

	Titles.helpBanner = love.graphics.newImage("img/help.png")
	Titles.creditsBanner = love.graphics.newImage("img/credits.png")

	Titles.wheelImg = getQuad("wheel")
	Titles.backArrow = getQuad("backarrow")
	Titles.restart()
end

function Titles.restart()
	Titles.titleOpacity = 1
	Titles.overlayOpacity = 1
	Titles.overlayColor = 0
	Titles.underlayOpacity = 0
	Titles.helpPhase = 0
	Titles.endTimer = 0
	Titles.optionsMsgOpacity = 0
	Titles.optionsMsgTimer = 0

	Titles.helpOpacity = 0
	Titles.creditsOpacity = 0
	Titles.resetTimer = 0	

	Titles.characterOpacity = 1
	Titles.otherOpacity = 0
	Titles.optionsOpacity = 0

    Titles.callOpacity = 0
    Titles.callTimer = 0
end

function Titles.update( dt )
	if game.mode == 1 then
		Titles.overlayOpacity = decreaseExponential(dt, Titles.overlayOpacity, 0.99)
		Titles.characterOpacity = increaseExponential(dt, Titles.characterOpacity, 0.98)
	end

	if game.mode == 2  then
		Titles.titleOpacity = decreaseExponential(dt, Titles.titleOpacity, 0.95)
		if Titles.helpPhase < 4 then
			Titles.helpOpacity = increaseExponential(dt, Titles.helpOpacity, 0.97)
			if Titles.helpOpacity > 0.99 then
				Titles.helpPhase = Titles.helpPhase + 1
				Titles.helpOpacity = 0.00011
			end	
		else
			Titles.helpOpacity = 0
			game.showingHelp = false
		end

		if Titles.optionsMsgTimer < 16 then
			Titles.optionsMsgTimer = Titles.optionsMsgTimer + dt
			if Titles.optionsMsgTimer > 12 then
				Titles.optionsMsgOpacity = increaseExponential(dt, Titles.optionsMsgOpacity, 0.97)
			end
		else
			Titles.optionsMsgOpacity = decreaseExponential(dt, Titles.optionsMsgOpacity, 0.97)
		end

		if Titles.callTimer > 0 then
			Titles.callTimer = Titles.callTimer - dt
			if Titles.callTimer < 3 then
				Titles.callOpacity = decreaseExponential(dt, Titles.callOpacity, 0.97)
			elseif Titles.callTimer < 5 then
				Titles.callOpacity = increaseExponential(dt, Titles.callOpacity, 0.98)
			elseif Titles.callTimer > 5 and Friends.firstPulse then
				Titles.callTimer = 0
			end

			if Titles.callTimer <= 0 then
				Titles.callOpacity = 0
				if not Friends.firstPulse then
					Titles.callTimer = 30
				end
			end
		end

		Titles.overlayOpacity = decreaseExponential(dt, Titles.overlayOpacity, 0.99)
		Titles.otherOpacity = increaseExponential(dt, Titles.otherOpacity, 0.98)
		Titles.characterOpacity = increaseExponential(dt, Titles.characterOpacity, 0.98)
	end

	if game.mode == 3 then
		Titles.characterOpacity = decreaseExponential(dt, Titles.characterOpacity, 0.98)
	end

	if game.mode == 1 and love.mouse.isDown("l") then
		game.setMode(2)
	end

	if game.mode == 2 and Friends.checkAttachedFriends() then
		game.setMode(3)
	end

	if game.mode == 3 then
		Titles.overlayColor = 1
		Titles.titleOpacity = decreaseExponential(dt, Titles.titleOpacity, 0.95)
		Titles.endTimer = Titles.endTimer + dt
		if Titles.endTimer < 4 then
			Titles.overlayOpacity = increaseExponential(dt, Titles.overlayOpacity, 0.97)
		else
			Titles.overlayOpacity = decreaseExponential(dt, Titles.overlayOpacity, 0.99)
			Titles.underlayOpacity = 1
			if Titles.resetTimer == 0 then				
				Titles.creditsOpacity = 1
				if love.mouse.isDown("l") then
					Titles.resetTimer = dt
				end
			else
				Titles.creditsOpacity = decreaseExponential(dt, Titles.creditsOpacity, 0.95)
				Titles.resetTimer = Titles.resetTimer + dt
				if Titles.resetTimer >= 1.2 then
					game.restart = true 
				end
			end
		end
		Titles.fadeout(dt)
	end

	if game.mode == 4 then
		Titles.overlayOpacity = decreaseExponential(dt, Titles.overlayOpacity, 0.99)
	end
end

function Titles.fadeout( dt )
	love.audio.setVolume( love.audio.getVolume() * 0.99 )
end

function Titles.draw()
	if Titles.underlayOpacity > 0 then
		love.graphics.setColor(0, 0, 0)
		love.graphics.rectangle("fill",0, 0, love.graphics.getWidth()/VideoModes.scale[1], love.graphics.getHeight()/VideoModes.scale[2])
	end


	if Titles.titleOpacity > 0.001 then
		love.graphics.setColor(255, 255, 255, 255 * Titles.titleOpacity * Titles.characterOpacity)
		love.graphics.draw(Batch:getImage(),Titles.titleBanner, 70/game.zoom, (love.graphics.getHeight()/VideoModes.zoom[2] - 200)/game.zoom, 0, 1 / game.zoom, 1 / game.zoom)
	end

	if Titles.helpOpacity > 0.0001 then
		love.graphics.push()
		love.graphics.scale(1/game.zoom,1/game.zoom)
		local txt_relax = "RELAX"
		local txt_follow = "FOLLOW YOUR HEART"
		local txt_find = "FIND NEW FRIENDS"

		local vposUp = 136
		local vposDn = vposUp
		local hposUp  = 512
		local hposDn = hposUp

		local txtUp,txtDn = nil,nil

		if Titles.helpPhase == 0 then
			txtDn = txt_relax
		elseif Titles.helpPhase == 1 then
			txtUp = txt_relax
			txtDn = txt_follow
			vposDn = vposUp + 100
		elseif Titles.helpPhase == 2 then
			txtUp = txt_follow
			txtDn = txt_find
			vposUp = vposUp + 100
			vposDn = vposUp + 100
		elseif Titles.helpPhase == 3 then
			txtUp = txt_find
			vposUp = vposUp + 200
		end

		if txtUp then
			love.graphics.setColor(255, 255, 255, 255 * (1-Titles.helpOpacity) * Titles.characterOpacity)
			hposUp = (hposUp - game.font:getWidth(txtUp)/2)
			love.graphics.print(txtUp, hposUp, vposUp)
		end

		if txtDn then
			love.graphics.setColor(255, 255, 255, 255 * Titles.helpOpacity * Titles.characterOpacity)
			hposDn = (hposDn - game.font:getWidth(txtDn)/2)
			love.graphics.print(txtDn, hposDn, vposDn)
		end
		love.graphics.pop()
	end

	if Titles.callOpacity > 0.0001 then
		love.graphics.push()
		love.graphics.scale(1/game.zoom,1/game.zoom)
		local txt_call = "MAKE THE CALL"

		love.graphics.setColor(255, 255, 255, 255 * Titles.callOpacity * Titles.characterOpacity)
		local hpos = (512 - game.font:getWidth(txt_call)/2)
		love.graphics.print(txt_call, hpos, 136)

		love.graphics.pop()
	end

	if Titles.optionsMsgOpacity > 0.001 then
		love.graphics.push()
		love.graphics.scale(0.4/game.zoom,0.4/game.zoom)
		love.graphics.setColor(255, 255, 255, 255 * Titles.optionsMsgOpacity * Titles.characterOpacity)
		love.graphics.print("F1 - Options", 2.5*(love.graphics.getWidth()/VideoModes.zoom[1] - 150), 10)
		love.graphics.pop()
	end

	if Titles.creditsOpacity > 0.001 then
		love.graphics.setColor(255, 255, 255, 255 * Titles.creditsOpacity)
		love.graphics.draw(Titles.creditsBanner,
			love.graphics.getWidth()/VideoModes.scale[1]/2 - Titles.creditsBanner:getWidth() * 0.35 / game.zoom,
		 	love.graphics.getHeight()/VideoModes.scale[2]/2 - Titles.creditsBanner:getHeight() * 0.4 / game.zoom,
			0, 0.7 / game.zoom, 0.7 / game.zoom)
	end

	if Titles.overlayOpacity > 0.001 then
		love.graphics.setColor(255 * Titles.overlayColor, 255 * Titles.overlayColor, 255 * Titles.overlayColor, 255 * Titles.overlayOpacity)
		love.graphics.rectangle("fill",0,0,love.graphics.getWidth()/VideoModes.scale[1], love.graphics.getHeight()/VideoModes.scale[2])
	end
end
