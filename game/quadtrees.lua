-- naive implementation
QuadTrees = QuadTrees or {}
QuadTrees.totalCount = 1

function QuadTrees.init(screenSize)
	QuadTrees.dimensions = { screenSize[1], screenSize[2] }
    QuadTrees.itemLimit = 6
	return QuadTrees.newQuad( 0,0,screenSize[1],screenSize[2] )
end

function QuadTrees.insert(quad,point,element)  
  if quad.childrenCount == 0 and table.getn(quad.elements) < QuadTrees.itemLimit then
    table.insert(quad.elements, element)
    element.parentQuad = quad
    element.pos = {point[1], point[2]}
  else
    -- split
    for i = 1,#quad.elements do
    	local v = quad.elements[i]
    	QuadTrees.createChildAndInsert(quad, v.pos, v)
    end
    quad.elements = {}
    QuadTrees.createChildAndInsert(quad, point, element)
    
  end
end


function QuadTrees.move(quad, newPosition, element)
    if not QuadTrees.pointInBox(newPosition, element.parentQuad) then
        local formerParent = element.parentQuad
        QuadTrees.insert(quad, newPosition, element)
        QuadTrees.removeElement(formerParent, element)
    end
end

function QuadTrees.eval(quad, func, x1, y1, x2, y2)
	-- split horizontally
	if x1 < 0 then
		QuadTrees.eval(quad, func, x1 + QuadTrees.dimensions[1], y1, QuadTrees.dimensions[1], y2)
		QuadTrees.eval(quad, func, 0, y1, x2, y2 )
		return
	elseif x2 > QuadTrees.dimensions[1] then
		QuadTrees.eval(quad, func, x1, y1, QuadTrees.dimensions[1], y2)
		QuadTrees.eval(quad, func, 0, y1, x2 - QuadTrees.dimensions[1], y2)
		return
	end

	-- split vertically
	if y1 < 0 then
		QuadTrees.eval(quad, func, x1, y1 + QuadTrees.dimensions[2], x2, QuadTrees.dimensions[2] )
		QuadTrees.eval(quad, func, x1, 0, x2, y2 )
		return
	elseif y2 > QuadTrees.dimensions[2] then
		QuadTrees.eval(quad, func, x1, y1, x2, QuadTrees.dimensions[2] )
		QuadTrees.eval(quad, func, x1, 0, x2, y2 - QuadTrees.dimensions[2])
		return
	end

	QuadTrees.evalQuad(quad, func, x1, y1, x2, y2)
end
-------------------------------------------------------

function QuadTrees.newQuad( coordx1, coordy1, coordx2, coordy2 )
	return { 
		box = {coordx1, coordy1, coordx2, coordy2 },
		size = {coordx2 - coordx1, coordy2 - coordy1 },
		children = {nil,nil,nil,nil}, 
		elements = {},
		parent = nil,
		childrenCount = 0,
		index = 0
	}
end

function QuadTrees.createChildAndInsert(quad, point, element)
  local ci = QuadTrees.indexFromPosition(point,quad)
  if not quad.children[ci] then
    local x = (ci - 1)%2
    local y = math.floor((ci-1)/2)
    local hs,vs = quad.size[1]*0.5, quad.size[2]*0.5
    quad.children[ci] = QuadTrees.newQuad( quad.box[1] + hs*x, quad.box[2] + vs*y, quad.box[1]+hs*(x+1), quad.box[2]+vs*(y+1) )
    quad.children[ci].parent = quad
    quad.children[ci].index = ci
    quad.childrenCount = quad.childrenCount + 1
    QuadTrees.totalCount = QuadTrees.totalCount + 1
  end
  QuadTrees.insert(quad.children[ci], point, element)
end

function QuadTrees.removeElement(quad, element)
	removeOne(quad.elements, element)
	if table.getn(quad.elements) == 0 then
		QuadTrees.purge(quad)			
	end
end

function QuadTrees.purge(quad)
	if quad.childrenCount == 0 and quad.parent then
		quad.parent.children[quad.index] = nil
		quad.parent.childrenCount = quad.parent.childrenCount - 1
		QuadTrees.totalCount = QuadTrees.totalCount - 1
		QuadTrees.purge(quad.parent)
	end
end

function QuadTrees.evalQuad(quad, func, x1, y1, x2, y2)
  if quad.childrenCount == 0 then
		for i = 1,#quad.elements do
			func(quad.elements[i])
		end
	else
		for i=1,4 do
			if quad.children[i] and QuadTrees.intersects(quad.children[i].box, x1, y1, x2, y2) then
				QuadTrees.evalQuad(quad.children[i], func, x1, y1, x2, y2)
			end
		end		
	end
end


function QuadTrees.pointInBox(point,quad)
	return point[1]>=quad.box[1] and point[1]<quad.box[3] and point[2]>=quad.box[2] and point[2]<quad.box[4]
end

function QuadTrees.indexFromPosition(point, quad)
	local px, py = point[1] - quad.box[1], point[2] - quad.box[2]
	local x,y = 1,0
	if px >= quad.size[1]*0.5 then x = 2 end
	if py >= quad.size[2]*0.5 then y = 2 end
	return x + y 
end

function QuadTrees.intersects(box1, xb1,yb1,xb2,yb2)
	return box1[1]<=xb2 and box1[3]>xb1 and box1[2]<=yb2 and box1[4]>yb1
end

