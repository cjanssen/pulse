RhythmDisplay = RhythmDisplay or {}

function RhythmDisplay.init()
    RhythmDisplay.dotStates = {
        begin = 1,
        valid = 2,
        fading = 3,
        missed = 4,
    }

    RhythmDisplay.dispStates = {
        hide = 1,
        locked = 2,
        normal = 3,
    }
    -- RhythmDisplay.states = {
    --     begin = 1,
    --     valid = 3,
    --     fading = 4,

    --     show = 5,
    --     locked = 2,
    --     hide = 6
    -- }
    -- RhythmDisplay.reset()
    RhythmDisplay.reset()
end

function RhythmDisplay.reset()
    RhythmDisplay.data = {
        opacity = 0,
        opUp = 0.935,
        opDown = 0.95,
        state = RhythmDisplay.dispStates.hide,
        phase = 0,
        radius = 128,
        newRadius = 128,
        currentCircle = nil,
        color = {255,255,255},
        list = {},
        life = 0,
    }
    -- RhythmDisplay.data = {
    --     opacity = 0,
    --     life = 0,
    --     state = RhythmDisplay.states.hide,
    --     phase = 0,
    --     radius = 128,
    --     newRadius = 128,
    --     currentCircle = nil,
    --     color = {255,255,255},
    --     lastId = 0,
    --     list = {},
    -- }
end

function RhythmDisplay.fadeAll()
    local Rl = RhythmDisplay.data.list
    for i=1,#Rl do
        if Rl[i].state ~= RhythmDisplay.dotStates.missed then
            Rl[i].state = RhythmDisplay.dotStates.fading
        end
    end

end

function RhythmDisplay.resetHeart()
    local R = RhythmDisplay.data
    R.life = player.heartPeriod * 1.5
end

function RhythmDisplay.show()
    RhythmDisplay.preparePounce(PounceControl.data.sequenceLen)
    RhythmDisplay.resetHeart()
end

function RhythmDisplay.hide()
    RhythmDisplay.fadeAll()
    local R = RhythmDisplay.data
    R.state = RhythmDisplay.dispStates.hide
end

function RhythmDisplay.reshow()
    if game.anchorPoint then
        RhythmDisplay.preparePounce(PounceControl.data.sequenceLen)
        RhythmDisplay.resetHeart()
    end
end

function RhythmDisplay.preparePounce(len)
    local R = RhythmDisplay.data
    R.state = RhythmDisplay.dispStates.locked
    -- R.phase = 0

    RhythmDisplay.createDots(len)
end

function RhythmDisplay.startPounce(len)
    local R = RhythmDisplay.data
    RhythmDisplay.resetHeart()
    -- R.phase = 0

    if R.state ~= RhythmDisplay.dispStates.locked then
        RhythmDisplay.createDots(len)
    end
    R.state = RhythmDisplay.dispStates.normal
end

function RhythmDisplay.createDots(len)
    local R = RhythmDisplay.data
    R.color = {255,255,255}

    -- first "clean" list
    RhythmDisplay.fadeAll()

    for i = 1,len do
        local newState = i==1 and RhythmDisplay.dotStates.valid or RhythmDisplay.dotStates.begin
        local newRadius = i~=len and 6 or 8
        table.insert(R.list, {
            phase = (i-1),
            opacity = 1,
            radius = newRadius,
            state = newState
            })
    end
end

function RhythmDisplay.continuePounce()
    local R = RhythmDisplay.data
    RhythmDisplay.resetHeart()
    if R.currentCircle then
        R.currentCircle.state = RhythmDisplay.dotStates.valid
    else
        RhythmDisplay.fadeAll()
    end
    R.currentCircle = nil
end

function RhythmDisplay.finishPounce()
    local R = RhythmDisplay.data
    RhythmDisplay.resetHeart()
    R.color = {203,255,203}
    if R.currentCircle then
        R.currentCircle.state = RhythmDisplay.dotStates.valid
    end
    R.state = RhythmDisplay.dispStates.hide
    R.currentCircle = nil
end

function RhythmDisplay.missedPounce()
    local Rl = RhythmDisplay.data.list
    for i=1,#Rl do
        if Rl[i].state == RhythmDisplay.dotStates.valid or Rl[i].state == RhythmDisplay.dotStates.begin then
            Rl[i].state = RhythmDisplay.dotStates.missed
        end
    end
    local R = RhythmDisplay.data
    R.state = RhythmDisplay.dispStates.hide
    R.color = {255,192,192}
end

function RhythmDisplay.update(dt)
    local R = RhythmDisplay.data

    R.newRadius = 128 + 16 * #player.followers
    if R.newRadius > R.radius then
        R.radius = math.min(R.newRadius, R.radius + dt * 4)
    elseif R.newRadius < R.radius then
        R.radius = math.max(R.newRadius, R.radius - dt * 16)
    end

    if R.state == RhythmDisplay.dispStates.locked then
        R.phase = (R.phase - dt * 0.35 + dt / player.heartPeriod * 0.2 * math.pi) % (2*math.pi) 
        R.opacity = increaseExponential(dt, R.opacity, R.opUp)
        for i = #R.list,1,-1 do
            local point = R.list[i]
            if point.state == RhythmDisplay.dotStates.fading or point.state == RhythmDisplay.dotStates.missed then
                point.opacity = decreaseExponential(dt, point.opacity, 0.9)
            end
        end
    else
        if R.state == RhythmDisplay.dispStates.normal  then
            R.phase = (R.phase - dt*0.35) % (2*math.pi)
            R.opacity = increaseExponential(dt, R.opacity, R.opUp)
            R.life = R.life - dt
            if R.life <= dt then
                R.state = RhythmDisplay.dispStates.hide
            end
        elseif R.state == RhythmDisplay.dispStates.hide then
            R.phase = (R.phase - dt*0.35) % (2*math.pi)
            R.opacity = decreaseExponential(dt, R.opacity, R.opDown)
        end

        for i = #R.list,1,-1 do
            local point = R.list[i]
            point.phase = point.phase - dt / player.heartPeriod
            if point.state == RhythmDisplay.dotStates.begin then
                if point.phase*player.heartPeriod >= Circles.params.beginWait - player.heartPeriod and
                    point.phase*player.heartPeriod <= Circles.params.endWait - player.heartPeriod then
                        R.currentCircle = point
                end

                if point.phase*player.heartPeriod < Circles.params.beginWait - player.heartPeriod then
                    R.currentCircle = nil
                    RhythmDisplay.fadeAll()
                end
            end

            if point.state == RhythmDisplay.dotStates.fading or point.state == RhythmDisplay.dotStates.missed then
                point.opacity = decreaseExponential(dt, point.opacity, 0.9)
            end

            if point.state == RhythmDisplay.dotStates.valid then
                point.opacity = decreaseExponential(dt, point.opacity, 0.985)
            end

            if point.opacity == 0 then
                table.remove(R.list, i)
            end
        end

    end
end

function RhythmDisplay.draw()
    local R = RhythmDisplay.data
    local basicOp = 160 * R.opacity * Titles.characterOpacity * Circles.params.globalAlpha
    love.graphics.push()
    love.graphics.setColor(R.color[1],R.color[2],R.color[3], basicOp)
    -- love.graphics.scale(0.5/game.zoom,0.5/game.zoom)
    love.graphics.setLineWidth(3/game.zoom)

    local x0, y0 = player.screenPos[1], player.screenPos[2]
    love.graphics.circle("line", 
        x0+math.cos(R.phase)*R.radius, 
        y0+math.sin(R.phase)*R.radius, 
        10/game.zoom, 30)

    for i=1,#R.list do
        local point = R.list[i]
        local color = {255,255,255}
        if point.state == RhythmDisplay.dotStates.valid then color = {203,255,203} end
        if point.state == RhythmDisplay.dotStates.fading then color = {255,203,203} end
        if point.state == RhythmDisplay.dotStates.missed then color = {255,164,164} end
        love.graphics.setColor(color[1], color[2], color[3], point.opacity * basicOp)
        local ang = R.phase - point.phase * 0.2 * math.pi 
        love.graphics.circle("fill",
            x0 + R.radius*math.cos(ang),
            y0 + R.radius*math.sin(ang),
            point.radius/game.zoom,24)
    end

    love.graphics.pop()
end
