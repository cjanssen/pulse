Plankton = Plankton or {}


function Plankton.init()
	Plankton.list = {}
	Plankton.planktonQuads = {
		getQuad("plankton_500_1"),
		getQuad("plankton_500_2"),
		getQuad("plankton_500_3"),
		getQuad("plankton_500_4"),
		getQuad("plankton_500_5"),
		getQuad("soft_1"),
		getQuad("soft_2"),
		getQuad("soft_3"),
		getQuad("round_1"),
		getQuad("round_2"),
		getQuad("round_3"),
		getQuad("sharp_1"),
		getQuad("sharp_2"),
		getQuad("sharp_3")
	}

	Plankton.makeQuadTrees()
	Plankton.updatedCount = 0
	Plankton.updatedList = {}
	Plankton.toDelete = {}

	-- in coroutine? -> do it radially from player position -> but player will be moving!
	Plankton.updateCount()

	Plankton.batches = {}
	for i=1,3 do 
		Plankton.batches[i] = getNewBatch()
	end

	Plankton.batches[1]:setColor(255, 255, 255, 192)
	Plankton.batches[2]:setColor(192, 192, 255, 144)
	Plankton.batches[3]:setColor(128, 128, 255, 96)
	
	--- fixed layers
	fixedlayersenabled = true
	if fixedlayersenabled and not Plankton.fixedLayers then
		Plankton.createFixedLayers()
	 -- Plankton.fixedLayers = {}
	end
end

function Plankton.updateCount()
	if not Plankton.list then
		return
	end

	function setNumberTo(number)
		if table.getn(Plankton.list) > number then
			for i=number+1, table.getn(Plankton.list) do
				Plankton.killPlankton(Plankton.list[i])
			end
		elseif table.getn(Plankton.list) < number then
			local diff = number - table.getn(Plankton.list)
			for i=1,diff do
				Plankton.addOne()
			end
		end
	end

	setNumberTo(200)
	-- setNumberTo(1)
end

function Plankton.addOne()
	local i = table.getn(Plankton.list)

	--local layerChoice = math.random(3)
	local layerChoice
	if math.random() < 5/7 then
		layerChoice = 1
	elseif math.random() < 2/3 then
		layerChoice = 2
	else
		layerChoice = 3
	end

	local imgChoice = math.random(table.getn(Plankton.planktonQuads))
	if layerChoice == 1 then  -- avoid second half of the table if in foreground
		imgChoice = math.random(8) -- hardcoded 8
	end

	
	local v = {
			number = i,
			pos =  { math.random(game.worldSize[1]), math.random(game.worldSize[2]) },
			screenPos = { -5000, -5000 },
			vel = { math.random()*150 - 75, math.random()*150-75 },
			acc = { 0, 0 },
			imgIndex = imgChoice,
			rotationSpeed = math.random()*6 - 3,
			angle = math.random(360),
			layer = layerChoice,
			scaleFactor = 1 + (math.random()-0.5),
		}
	mapCoords(v)

	table.insert(Plankton.list, v)
	QuadTrees.insert(Plankton.layerQuads[v.layer], v.pos, v)
end


function Plankton.createFixedLayers()
	local function newFixedLayer(layer)
		local lw,lh = game.worldSize[1]/3, game.worldSize[2]/3
		local batch = getNewBatch()

		local layercol
		if layer == 3 then layercol = {128, 128, 255, 96} end
		if layer == 2 then layercol = {192, 192, 255, 144} end
		if layer == 1 then layercol = {255, 255, 255, 192} end

		love.graphics.setColor(255,255,255)
		batch:setColor(unpack(layercol))

		local cw,ch = 650,650

		-- draw individual plankton
		for yy = 0,lh,ch do
			for xx = 0,lw,cw do

				local imgChoice = math.random(table.getn(Plankton.planktonQuads))
				if layer == 1 then  -- avoid second half of the table if in foreground
					imgChoice = math.random(8) -- hardcoded 8
				end

				local image = Plankton.planktonQuads[imgChoice]
				local ix,iy,iw,ih = image:getViewport()
				local xr,yr = cw-iw,ch-ih
				if xx+iw*0.5+xr<lw and yy+ih*0.5+yr<lh then
					local x,y = xx + iw*0.5 + math.random() * (cw-iw), yy + ih*0.5 + math.random() * (ch-ih)
					local angle = math.random()*math.pi*2
					local scaleFactor = 1 + (math.random()-0.5),
					batch:add(image, x, y, angle, scaleFactor, scaleFactor, iw*0.5, ih*0.5)
				end
			end
		end


		Plankton.maxLayerRotSpeed = 0.8


		return {
			visible = true,
			batch = batch,
			scaleFactor = 3 * game.layerFactors[4-layer],
			rotationSpeed = (math.random()-0.5) * Plankton.maxLayerRotSpeed*2,
			angle = math.random(360),
			pos = {0,0},
			color = layercol,
			width = lw,
			height = lh,
			oldzoom = game.zoom
		}
	end

	Plankton.fixedLayers = {}
	-- Plankton.fixedLayers[1] = { visible = false }
	for i=1,3 do
		Plankton.fixedLayers[i] = {}
		if i>1 then
			for j=1,3 do
				table.insert(Plankton.fixedLayers[i], newFixedLayer(i))
			end
		end
	end
end

function Plankton.makeQuadTrees()
	local cellSizes = {{200,200}, {400,400}, {800,800}}
	Plankton.layerQuads = {}
	table.insert(Plankton.layerQuads, QuadTrees.init(game.worldSize, cellSizes[1]))
	table.insert(Plankton.layerQuads, QuadTrees.init(game.worldSize, cellSizes[2]))
	table.insert(Plankton.layerQuads, QuadTrees.init(game.worldSize, cellSizes[3]))
	for i,v in ipairs(Plankton.list) do
		QuadTrees.insert(Plankton.layerQuads[v.layer], v.pos, v)
	end
end

function Plankton.update( dt )
	Plankton.removeOld()
	if game.quadPlankton then	
		Plankton.updatedCount = 0
		Plankton.updateInQuads(dt)
		for i = 1,Plankton.updatedCount do
			Plankton.updateQuadCoords(Plankton.updatedList[i])
		end
	else
		for i,v in ipairs(Plankton.list) do
			Plankton.updatePlankton( v, dt )
		end
	end

	Plankton.updateFixedLayers(dt)
end

function Plankton.updateFixedLayers(dt)
	if fixedlayersenabled then
		for i=1,#Plankton.fixedLayers do
			for j=1,#Plankton.fixedLayers[i] do
				local fl = Plankton.fixedLayers[i][j]
				if fl.visible then
					fl.pos = {
						fl.pos[1] - game.screenVel[1]*game.layerFactors[i]*dt,
						fl.pos[2] - game.screenVel[2]*game.layerFactors[i]*dt
					}
					if fl.oldzoom ~= game.zoom then
						fl.pos = { player.screenPos[1] + fl.pos[1]-(player.screenPos[1])*game.zoom/fl.oldzoom,
									player.screenPos[2] + fl.pos[2]-(player.screenPos[2])*game.zoom/fl.oldzoom}
						fl.oldzoom = game.zoom
					end
					fl.angle = (fl.angle + fl.rotationSpeed * dt) % 360
					--fl.rotationSpeed = math.max(-Plankton.maxLayerRotSpeed, math.min(fl.rotationSpeed + (math.random()-0.5)*0.8, Plankton.maxLayerRotSpeed))
				end
			end
		end
	end
end

function Plankton.draw()
	for i=1,3 do Plankton.batches[i]:clear() end
	if game.quadPlankton then
		for i = 1,Plankton.updatedCount do
			Plankton.drawPlankton(Plankton.updatedList[i])
		end
	else		
		for i,v in ipairs(Plankton.list) do
			Plankton.drawPlankton( v )
		end
	end
end

function Plankton.drawFixedLayers(layer)
	if fixedlayersenabled then
		for i=1,#Plankton.fixedLayers[layer] do
			local fl = Plankton.fixedLayers[layer][i]
			if not fl.visible then return end

			local ang = fl.angle*math.pi/180
			px,py = fl.pos[1],fl.pos[2]
			local rw,rh = fl.width*fl.scaleFactor,fl.height*fl.scaleFactor
			local sw,sh = game.screenWidth,game.screenHeight

			local function getLayerNdxs()
				local vx = {x = math.cos(-ang), y = math.sin(-ang)}
				local vy = {x = math.cos(-ang+math.pi/2),y = math.sin(-ang+math.pi/2)}
				
				local ox = -px * vx.x -py * vy.x
				local oy = -px * vx.y -py * vy.y

				local A = {x = ox,y = oy}
				local B = {
					x = ox + vx.x * sw,
					y = oy + vx.y * sw
				}
				local C = {
					x = ox + vy.x * sh,
					y = oy + vy.y * sh
				}
				local D = {
					x = ox + vx.x * sw + vy.x * sh,
					y = oy + vx.y * sw + vy.y * sh	
				}

				local minx = math.min(A.x,B.x,C.x,D.x)
				local maxx = math.max(A.x,B.x,C.x,D.x)
				local miny = math.min(A.y,B.y,C.y,D.y)
				local maxy = math.max(A.y,B.y,C.y,D.y)

				local ixfrom = math.floor(minx/rw)
				local ixto = math.floor(maxx/rw)
				local iyfrom = math.floor(miny/rh)
				local iyto = math.floor(maxy/rh)

				return ixfrom,ixto,iyfrom,iyto
			end

			ixfrom,ixto,iyfrom,iyto =  getLayerNdxs()

			local drx = {
				x = math.cos(ang)*rw,
				y = math.sin(ang)*rw
			}
			local dry = {
				x = math.cos(ang+math.pi/2)*rh,
				y = math.sin(ang+math.pi/2)*rh
			}

			local sx,sy = 0,0

			for iy=iyfrom,iyto do
				for ix=ixfrom,ixto do
					local x = sx + px + ix*drx.x + iy*dry.x
					local y = sy + py + ix*drx.y + iy*dry.y

					A = {x=x,y=y}
					B = {x=x+drx.x,y=y+drx.y}
					C = {x=x+dry.x,y=y+dry.y}
					D = {x=x+drx.x+dry.x,y=y+drx.y+dry.y}
					if math.max(A.x,B.x,C.x,D.x) > sx and
					   math.min(A.x,B.x,C.x,D.x) < sx+sw and
					   math.max(A.y,B.y,C.y,D.y) > sy and
					   math.min(A.y,B.y,C.y,D.y) < sy+sh then
					    fl.pos = {x,y}
						love.graphics.draw(fl.batch, x,y, ang, fl.scaleFactor, fl.scaleFactor)
					end
				end
			end
		end
	end
end

function Plankton.drawBatches()
	love.graphics.setColor(255,255,255)
	for i=#Plankton.batches,1,-1 do
		Plankton.drawFixedLayers(i)
		love.graphics.draw(Plankton.batches[i])
	end
end

function Plankton.updateInQuads(dt)
	function myUpdate(plankton)
		Plankton.updatePlankton(plankton, dt)
		Plankton.updatedCount = Plankton.updatedCount + 1
		Plankton.updatedList[Plankton.updatedCount] = plankton
	end

	for layer = 1,3 do
		local sw = (Options.width / VideoModes.scale[1] * 0.5 + 200) / game.layerFactors[layer]
		local sh = (Options.height / VideoModes.scale[2] * 0.5  + 200)  / game.layerFactors[layer]
		local sc = {game.layerCenters[layer][1] / game.layerFactors[layer], game.layerCenters[layer][2] / game.layerFactors[layer] }

		QuadTrees.eval(Plankton.layerQuads[layer], myUpdate, sc[1] - sw, sc[2] - sh, sc[1] + sw, sc[2] + sh )
	end
end


function Plankton.updatePlankton( plankton, dt )
	updateMoveFixedVel( plankton, dt )
	mapCoords( plankton )
end

function insertUnique(t,elem)
	for i,v in ipairs(t) do
		if v==elem then
			return
		end
	end
	table.insert(t,elem)
end

function Plankton.removeOld()
	for i,v in ipairs(Plankton.toDelete) do
			removeOne(Plankton.list, v)
			QuadTrees.removeElement(v.parentQuad,v)
	end
	Plankton.toDelete = {}
end

function Plankton.killPlankton( plankton )
	insertUnique(Plankton.toDelete, plankton)
end

function Plankton.updateQuadCoords( plankton )
	QuadTrees.move(Plankton.layerQuads[plankton.layer], plankton.pos, plankton)
end

function Plankton.drawPlankton( plankton )
	local image = Plankton.planktonQuads[plankton.imgIndex]
	local scale
	if plankton.layer == 2 then
		scale = plankton.scaleFactor
		Batch:setColor(192, 192, 255, 144)
	elseif plankton.layer == 3 then
		scale = 2 * plankton.scaleFactor
		Batch:setColor(128, 128, 255, 96)
	else
		scale = 0.5 * plankton.scaleFactor	
		Batch:setColor(255, 255, 255, 192)
	end

	local ix,iy,iw,ih = image:getViewport()
	-- Batch:add(image,
	-- 	plankton.screenPos[1], plankton.screenPos[2], plankton.angle * math.pi / 180, scale, scale, iw*0.5, ih*0.5)	
	Plankton.batches[plankton.layer]:add(image,
		plankton.screenPos[1], plankton.screenPos[2], plankton.angle * math.pi / 180, scale, scale, iw*0.5, ih*0.5)
end
