Options = Options or {}


function Options.load()
	Options.filename = "pulse_options.txt"

	Options.resIndex = 12
	Options.prepareMenu()

	-- default
	Options.fullscreen = true
	Options.width = 1024
	Options.height = 768
	Options.sfxSwitch = 1
	Options.sfxVol = 1
	Options.musicSwitch = 1
	Options.musicVol = 1
	Options.fullscreenWidth = Options.width
	Options.fullscreenHeight = Options.height
	Options.windowedWidth = Options.width
	Options.windowedHeight = Options.height


	Options.customZoom = 1 --0.75

	local validoption = VideoModes.getScreenDefaultOption(Options.fullscreen)
	if not validoption then
		Options.fullscreen = false
		validoption = VideoModes.getScreenDefaultOption(Options.fullscreen)
	end
	if not validoption then
		print("ERROR: could not find a valid video mode")
		Options.width = 0
		Options.height = 0
		love.event.push("quit")
		return
	else
		Options.width = validoption.width
		Options.height = validoption.height
	end

	Options.loadOptions()
end

function Options.prepareMenu()
	Options.pointerImg = getQuad("player_cell_4")
	local ix,iy,iw,ih = Options.pointerImg:getViewport()
	Options.pointerSize = {iw,ih}
	Options.pointerAngle = 0
	Options.initUI()
	Options.mouseX = 0
	Options.mouseY = 0
	Options.mousePressed = false
end

function Options.loadOptions()
	if not love.filesystem.exists(Options.filename) then
		return
	end

	local contents,size = love.filesystem.read(Options.filename)
	if size ~= 0 then
		Options.parseOptions(contents)
		if Options.width == 0 then
			love.filesystem.remove( Options.filename )
			Options.load()
		end
	end
	
end

function Options.save()
	local str = Options.serialize()
	love.filesystem.write(Options.filename,str,str:len())
end

function Options.parseOptions(str)
	Options.width = Options.readOption(str, "width", Options.width)
	Options.height = Options.readOption(str, "height", Options.height)
	Options.fullscreen = Options.readOption(str, "fullscreen", Options.fullscreen) ~= 0
	Options.sfxSwitch = Options.readOption(str, "sfxSwitch", Options.sfxSwitch)
	Options.sfxVol = Options.readOption(str, "sfxVol", Options.sfxVol) / 1000
	Options.musicSwitch = Options.readOption(str, "musicSwitch", Options.musicSwitch)
	Options.musicVol = Options.readOption(str, "musicVol", Options.musicVol) / 1000
	Options.fullscreenWidth = Options.readOption(str, "fullscreenWidth", Options.fullscreenWidth)
	Options.fullscreenHeight = Options.readOption(str, "fullscreenHeight", Options.fullscreenHeight)
	Options.windowedWidth = Options.readOption(str, "windowedWidth", Options.windowedWidth)
	Options.windowedHeight = Options.readOption(str, "windowedHeight", Options.windowedHeight)
end

function Options.readOption(str, optionString, defaultValue)
	local stBegin, stEnd = string.find(str, optionString)
	if not stBegin then
		return defaultValue
	end

	local valuePos, valueEnd = string.find(str, "%d+", stEnd)
	if not valuePos then
		return defaultValue
	end

	if string.match(string.sub(str, stEnd+1, valuePos-1), "%a+") then
		return defaultValue
	end

	return tonumber(string.sub(str,valuePos,valueEnd))
end

function Options.serialize()
	local str = ""
	str = str.."width "..Options.width.."\r\n"
	str = str.."height "..Options.height.."\r\n"
	str = str.."fullscreen "..(Options.fullscreen and 1 or 0).."\r\n"
	str = str.."sfxSwitch "..Options.sfxSwitch.."\r\n"
	str = str.."sfxVol "..math.ceil(Options.sfxVol*1000).."\r\n"
	str = str.."musicSwitch "..Options.musicSwitch.."\r\n"
	str = str.."musicVol "..math.ceil(Options.musicVol*1000).."\r\n"
	str = str.."fullscreenWidth "..Options.fullscreenWidth.."\r\n"
	str = str.."fullscreenHeight "..Options.fullscreenHeight.."\r\n"
	str = str.."windowedWidth "..Options.windowedWidth.."\r\n"
	str = str.."windowedHeight "..Options.windowedHeight.."\r\n"
	return str	
end

function Options.update( dt )
	if game.mode == 4 then
		Titles.characterOpacity = decreaseExponential(dt, Titles.characterOpacity, 0.95)
		Titles.otherOpacity = decreaseExponential(dt, Titles.otherOpacity, 0.95)
		Titles.optionsOpacity = increaseExponential(dt, Titles.optionsOpacity, 0.97)
	else
		Titles.optionsOpacity = decreaseExponential(dt, Titles.optionsOpacity, 0.95)
	end

	if Titles.optionsOpacity == 0 then
		return
	end

	Options.pointerAngle = Options.pointerAngle + 100 * dt
	if Options.pointerAngle > 360 then
		Options.pointerAngle = 0
	end

	local vertZoom = math.min(1, Options.width / Options.UIwidth / VideoModes.zoom[2] ) * Options.customZoom
	Options.mouseX = love.mouse.getX() / VideoModes.zoom[1] / vertZoom
	Options.mouseY = love.mouse.getY() / VideoModes.zoom[2] / vertZoom
	if game.mode == 4 then
		local mouseDown = Options.mousePressed
		for i,v in ipairs(Options.UIlist) do
			if mouseDown then
				Options.clickedUI(v, Options.mouseX, Options.mouseY)
			else
				Options.mouseoverUI(v, Options.mouseX, Options.mouseY)
			end

			Options.dragoverUI(v, Options.mouseX, Options.mouseY)

			Options.updateUI(v, dt)
		end
	end
end

function Options.registerClick()
	Options.mousePressed = true
end

function Options.unregisterClick()
	Options.mousePressed = false
end

function Options.draw()
	if Titles.optionsOpacity == 0 then
		return
	end

	local vertZoom = math.min(1, Options.width / Options.UIwidth / VideoModes.zoom[2] ) * Options.customZoom
	love.graphics.push()
	love.graphics.scale(vertZoom/game.zoom, vertZoom/game.zoom)
	love.graphics.setLineWidth(1)

	-- ui
	for i,v in ipairs(Options.UIlist) do
		Options.drawUI(v)
	end

	-- mouse pointer
	love.graphics.setColor(255,255,255,255 * Titles.optionsOpacity)
	-- love.graphics.draw(Options.pointerImg, Options.mouseX, Options.mouseY, 
	love.graphics.draw(Batch:getImage(),Options.pointerImg, Options.mouseX, Options.mouseY,
		Options.pointerAngle * math.pi / 180, 0.10, 0.10, 
		Options.pointerSize[1]*0.5, Options.pointerSize[2]*0.5 )

	love.graphics.pop()
end

function Options.toggle()
	if game.mode == 4 then
		game.setMode(Options.oldmode)
		Friends.warp()
	elseif game.mode ~= 3 then
		Options.oldmode = game.mode
		game.setMode(4)
		Friends.markPosition()
	end
end

-------------------------------- UI
-- 1 label
-- 2 button
-- 3 slider
-- 4 radio (on/off switch)
-- 5 pushbutton
function Options.newLabel(title_, x_, y_)
	table.insert(Options.UIlist, {
		uitype = 1,
		title = title_,
		x = x_,
		y = y_,
		w = game.font:getWidth(title_),
		h = game.font:getHeight()
	})
end

function Options.newButton(title_, x_, y_, optionid_)
	table.insert(Options.UIlist, {
		uitype = 2,
		title = title_,
		x = x_,
		y = y_,
		w = game.font:getWidth("<"..title_..">"),
		h = game.font:getHeight(),
		optionid = optionid_,
		selected = false,
		selectionColor = 0,
		hovered = false,
		hoverColor = 0
	})
end

function Options.newSlider(x_, y_, w_, optionid_)
	table.insert(Options.UIlist, {
		uitype = 3,
		x = x_,
		y = y_,
		w = w_,
		h = game.font:getHeight(),
		optionid = optionid_,
		hovered = false,
		hoverColor = 0,
		dragging = false
	})
end

function Options.newRadio(title_, x_, y_, optionid_)
	table.insert(Options.UIlist, {
		uitype = 4,
		title = title_,
		x = x_,
		y = y_,
		w = game.font:getWidth(title_.." off"),
		h = game.font:getHeight(),
		optionid = optionid_,
		hovered = false,
		hoverColor = 0
	})
end

function Options.newPushButton(title_, x_, y_, optionid_)
	table.insert(Options.UIlist, {
		uitype = 5,
		title = title_,
		x = x_,
		y = y_,
		w = game.font:getWidth(title_),
		h = game.font:getHeight(),
		optionid = optionid_,
		selected = false,
		selectionColor = 0,
		hovered = false,
		hoverColor = 0
	})
end

function Options.updateUI(elem, dt)
	-- active?
	if elem.uitype == 2 then
		if Options.getOption(elem.optionid) == 1 then
			elem.selected = true
		else
			elem.selected = false
		end
	elseif elem.uitype == 5 then
		if elem.hovered then
			elem.selected = true
		else
			elem.selected = false
		end
	end

	-- colors
	if elem.uitype == 2 or elem.uitype == 5 then
		if elem.selected then
			elem.selectionColor = increaseExponential(dt, elem.selectionColor, 0.9)
		else
			elem.selectionColor = decreaseExponential(dt, elem.selectionColor, 0.9)
		end
	end

	if elem.uitype == 2 or elem.uitype == 3 or elem.uitype == 4 or elem.uitype == 5 then
		if elem.hovered then
			elem.hoverColor = increaseExponential(dt, elem.hoverColor, 0.9)
		else
			elem.hoverColor = decreaseExponential(dt, elem.hoverColor, 0.9)
		end
	end
end

function Options.drawUI(elem)
	if elem.uitype == 1 then
		love.graphics.setColor(255,255,255,255 * Titles.optionsOpacity)
		love.graphics.print(elem.title, elem.x, elem.y)
	elseif elem.uitype == 2 then	
		love.graphics.setColor(192 + 63 * elem.hoverColor,
			192 + 63 * math.max(elem.selectionColor, elem.hoverColor),
			192 + 63 * elem.hoverColor,
			255 * Titles.optionsOpacity)
		if elem.selected then
			love.graphics.print("<"..elem.title..">", elem.x, elem.y)
		else
			love.graphics.print(" "..elem.title.." ", elem.x, elem.y)
		end
	elseif elem.uitype == 3 then
		love.graphics.setColor(192 + 63 * elem.hoverColor,
			192 + 63 * elem.hoverColor,
			192 + 63 * elem.hoverColor,
			255 * Titles.optionsOpacity)

		love.graphics.line(elem.x + 30, elem.y + elem.h*0.5, elem.x+elem.w-30, elem.y + elem.h*0.5)
		love.graphics.draw(Batch:getImage(),Options.pointerImg, elem.x + (elem.w-60) * Options.getOption(elem.optionid) + 30, elem.y + elem.h*0.5, 
			math.pi - Options.pointerAngle * math.pi / 180, 0.08, 0.08, 
			Options.pointerSize[1]*0.5, Options.pointerSize[2]*0.5)
	elseif elem.uitype == 4 then
		love.graphics.setColor(192,255,192,255 * Titles.optionsOpacity)		
		local txt = "on"
		if Options.getOption(elem.optionid) == 0 then
			love.graphics.setColor(192,192,192,255 * Titles.optionsOpacity)
			txt = "off"
		end
		love.graphics.print(elem.title.." "..txt, elem.x, elem.y)
	elseif elem.uitype == 5 then
		love.graphics.setColor(192 + 63 * elem.hoverColor,
			192 + 63 * math.max(elem.selectionColor, elem.hoverColor),
			192 + 63 * elem.hoverColor,
			255 * Titles.optionsOpacity)
			love.graphics.print(" "..elem.title.." ", elem.x, elem.y)
	end
end

function Options.mouseoverUI(elem, x, y)
	if elem.uitype == 1 then
		return
	end

	elem.hovered = false
	if x >= elem.x and x <= elem.x + elem.w and y >= elem.y and y <= elem.y + elem.h then
		elem.hovered = true
	end

	if elem.uitype == 3 and elem.dragging then
		
	end
end

function Options.clickedUI(elem, x, y)
	if elem.uitype == 1 then
		return
	end

	local inarea = false
	if x >= elem.x and x <= elem.x + elem.w and y >= elem.y and y <= elem.y + elem.h then
		inarea = true
	end

	if elem.uitype == 2 or elem.uitype == 5 then
		if inarea then
			Options.setOption(elem.optionid, 1)
		end
	end

	if elem.uitype == 3 then
		if inarea then
			elem.dragging = true
		end
	end

	if elem.uitype == 4 then
		if inarea then
			Options.setOption(elem.optionid, 1 - Options.getOption(elem.optionid))
		end
	end

	-- consume mouse
	Options.mousePressed = false
end

function Options.dragoverUI(elem, x, y)
	if elem.uitype == 3 and elem.dragging then
		if not love.mouse.isDown("l") then
			elem.dragging = false
		else
			local xdetect = math.min(elem.x + elem.w - 30, math.max(elem.x + 30, x))
			Options.setOption(elem.optionid, (xdetect - elem.x - 30)/(elem.w-60))
		end
	end
end

function Options.initUI()
	-- resolutions
	Options.UIlist = {}

	Options.newLabel("Resolution", 200, 100)
	local currentList = Options.fullscreen and VideoModes.fList or VideoModes.wList
	for i,v in ipairs(currentList) do
		local x = ((i-1) % 4) * 400 + 300
		local y = math.floor((i-1) / 4) * 100 + 200
		Options.newButton(v.width.." x "..v.height, x, y, Options.resIndex+i)
	end

	local yinc = math.floor(table.getn(currentList)/4) * 100 + 320

	-- fullscreen
	Options.newButton("Fullscreen", 600, yinc, 1)
	Options.newButton("Windowed", 1200, yinc, 2)

	-- sound
	Options.newRadio("Sound FX", 200, yinc + 160, 3)
	Options.newSlider(500, yinc + 160, 1400, 4)
	Options.newRadio("Music", 200, yinc+260, 5)
	Options.newSlider(500, yinc + 260, 1400, 6)

	-- exit!
	Options.newButton("Continue Playing", 400, yinc + 500, 7)
	Options.newButton("Start Over", 900, yinc + 500, 9)
	Options.newButton("Exit Game", 1400, yinc + 500, 8)

	Options.UIheight = yinc + 700
	Options.UIwidth = 2100
end

function Options.getOption(optionid)
	if optionid == 1 then
		return Options.fullscreen and 1 or 0
	elseif optionid == 2 then
		return not Options.fullscreen and 1 or 0
	elseif optionid == 3 then
		return Options.sfxSwitch
	elseif optionid == 4 then
		return Options.sfxVol
	elseif optionid == 5 then
		return Options.musicSwitch
	elseif optionid == 6 then
		return Options.musicVol
	elseif optionid == 7 then
		return 0
	elseif optionid == 8 then
		return 0
	elseif optionid == 9 then
		return 0
	elseif optionid > Options.resIndex then
		local currentList = Options.fullscreen and VideoModes.fList or VideoModes.wList
		if currentList[optionid-Options.resIndex].width == Options.width and 
			currentList[optionid-Options.resIndex].height == Options.height then
			return 1
		else
			return 0
		end
	end
end

function Options.setOption(optionid, number)
	if optionid == 1 then
		Options.fullscreen = true
		Options.width = Options.fullscreenWidth
		Options.height = Options.fullscreenHeight
		VideoModes.reloadScreen()
	elseif optionid == 2 then
		Options.fullscreen = false
		Options.width = Options.windowedWidth
		Options.height = Options.windowedHeight
		VideoModes.reloadScreen()
	elseif optionid == 3 then
		Options.sfxSwitch = number
	elseif optionid == 4 then
		Options.sfxVol = number
		Sounds.setVolumes()
	elseif optionid == 5 then
		Options.musicSwitch = number
		if Options.musicSwitch == 0 then
			Sounds.stopMusic()
		else
			Sounds.playMusic()
		end
	elseif optionid == 6 then
		Options.musicVol = number
		Sounds.setVolumes()
	elseif optionid == 7 then
		Options.toggle()
	elseif optionid == 8 then
		love.event.push("quit")
	elseif optionid == 9 then
		game.restart = true
		-- Options.toggle()
	elseif optionid > Options.resIndex then
		local currentList = Options.fullscreen and VideoModes.fList or VideoModes.wList
		Options.width = currentList[optionid-Options.resIndex].width
	    Options.height = currentList[optionid-Options.resIndex].height
	    if Options.fullscreen then
	    	Options.fullscreenWidth = Options.width
	    	Options.fullscreenHeight= Options.height
	    else
	    	Options.windowedWidth = Options.width
	    	Options.windowedHeight = Options.height
	    end
		VideoModes.reloadScreen()
	end
	Options.save()
end

