Circles = Circles or {}

local function getNewCircleColor(hue)
	local hue = (math.random(14)-7)*5+120
	-- print(hue)
	return {hsv2rgb(hue, 64, 255)}
end


local function createShader()
    local blurcode = [[
        uniform Image im2;
        vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
        {
            vec2 tc = texture_coords;
            vec2 disp = normalize(tc - vec2(0.5,0.5));
            vec4 im2col = vec4(0,0,0,0);
            float it = 6.0;
            for (float i = 0.0; i<it; i++) {
                im2col = im2col + Texel(texture, tc - disp * 0.0015 * i);
            }
            return color*im2col;
        }
    ]]
    local blurshader = love.graphics.newShader(blurcode)
    local timeslot,dotcount = 1/60, 60000
    local w,h = 1200,1200
    local cnv = love.graphics.newCanvas(w,h)
    coroutine.yield()
    love.graphics.setPointSize(2)
    while dotcount > 0 do
    	local t = love.timer.getTime()
	    love.graphics.setCanvas(cnv)
	    while love.timer.getTime() - t < timeslot and dotcount >0 do
	    	dotcount = dotcount - 1
	        local x,y = math.random(w-2),math.random(h-2)
	        local a = math.random(255)
	        love.graphics.setColor(255,255,255,a)
	    	love.graphics.point(x,y)
	    end
	    love.graphics.setCanvas()
	    coroutine.yield()
	end
    local cnv2 = love.graphics.newCanvas(w,h)
    love.graphics.setColor(255,255,255)
    love.graphics.setShader(blurshader)
    love.graphics.setCanvas(cnv2)
    love.graphics.draw(cnv)
    love.graphics.setCanvas()
    love.graphics.setShader()
    Circles.shaderImg = love.graphics.newImage(cnv2:getImageData())
    coroutine.yield()

    local shadercode = [[
        // coords = { xcenter, ycenter, rotation, unused }
        uniform vec2 center;
        uniform float rotation;
        uniform Image im2;
        vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
        {
            vec2 newCenter = center * vec2(1,-1) + vec2(0,love_ScreenSize.y);
            vec2 sc = screen_coords + love_ScreenSize.xy - newCenter;
            vec2 tc = sc/2 / love_ScreenSize.xy;
            vec2 oc = (tc-vec2(0.5,0.5)) * 0.75;
            vec2 nc = vec2(oc.x * cos(rotation) + oc.y * sin(rotation), -oc.x * sin(rotation) + oc.y * cos(rotation));
            vec4 im2col = Texel(im2, nc + vec2(0.5, 0.5));
            return color*vec4(1,1,1,im2col.x);
        }
    ]]

    Circles.shader = love.graphics.newShader(shadercode)
    Circles.shader:send("im2",Circles.shaderImg)
end

local co_update
local function continueShader()
	if not co_update then
		co_update = coroutine.create(createShader)
	end
	if coroutine.status(co_update) ~= "dead" then
		coroutine.resume(co_update)
	end
end

function Circles.init()
	Circles.list = {}

	Circles.ctypes = {
		playerNote = 1,
		friendFire = 2,
		friendEcho = 3,
		strangerReply = 4,
		angryStranger = 5
	}

	Circles.stages = {
		grow = 1,
		wait = 2,
		shrink = 3,
		vanish = 4,
		explode = 5,
	}

	Circles.results = {
		continued = 1,
		exploded = 2,
		misfired = 3
	}
	
	Circles.params = {
		growthspeed = 150,
		shrinkspeed = -100,
		vanishspeed = 200,
		explodespeed = 200,
		explodetwin = 270,
		toleranceTime = 0.2,
		lifeTime = 3,
		currentColor = getNewCircleColor(),
		startSeq = false,
		globalAlpha = 1,
		incAlpha = false,
		patience = 0,
		initialPatience = 15,
	}


	local hearttime = player.heartPeriod
	Circles.params.growthrate = (Circles.params.shrinkspeed - Circles.params.growthspeed) / (Circles.params.toleranceTime*hearttime)
	Circles.params.beginWait = hearttime * (1 - Circles.params.toleranceTime * 0.5) 
	Circles.params.endWait = hearttime * (1 + Circles.params.toleranceTime * 0.5)

	-- math.randomseed(os.time())
	-- createShader()
end


function Circles.restart()
	Circles.list = {}
	
	Circles.params.currentColor = getNewCircleColor()
	Circles.params.globalAlpha = 1
end

function Circles.newCircle(entity,circleType)
	local p = entity.pos or {0,0}
	local circ = {
		pos = {p[1],p[2]},
		screenPos = {entity.screenPos[1],entity.screenPos[2]},
		layer = 1,
		r = 10,
		angle = math.random()*math.pi*2,
		growth = Circles.params.growthspeed,
		alpha = 1,
		life = 0,
		stage = Circles.stages.grow,
		cType = circleType,
		baseColor = Circles.params.currentColor,
		baseAlpha = 1,
		follows = entity
	}

	if circ.cType ~= Circles.ctypes.playerNote then
		circ.stage =  Circles.stages.vanish
	end

	if circ.cType == Circles.ctypes.friendFire then
		circ.baseAlpha = 1 --0.8
	elseif circ.cType == Circles.ctypes.friendEcho then
		-- nothing
	elseif circ.cType == Circles.ctypes.strangerReply then
		circ.baseColor = {255,128,255}
	elseif circ.cType == Circles.ctypes.angryStranger then
		circ.baseColor = {255,196,128}
	end

	mapCoords(circ)
	table.insert(Circles.list, circ)
	return circ
end

function Circles.prepareShader()
	if not Circles.shader then
		continueShader()
	end
end

function Circles.update( dt )
	local hasPlayerCircle = false
	for i=1,#Circles.list do
		if Circles.list[i].cType == Circles.ctypes.playerNote then
			hasPlayerCircle = true
			break
		end
	end


	for i=#Circles.list,1,-1 do
		if not Circles.updateCircle(Circles.list[i], dt) then
			table.remove(Circles.list,i)
		end
	end

	if not hasPlayerCircle then
		Circles.params.startSeq = true
	end

	if Circles.params.incAlpha then
	 	Circles.params.globalAlpha = increaseExponential(dt, Circles.params.globalAlpha, 0.92)
	 	if Circles.params.globalAlpha == 1 then
	 		Circles.params.incAlpha = false
	 	end
	end


	table.sort(Circles.list, function(a,b) return a.stage < b.stage end)
end

function Circles.updateCircle( circle, dt )
	-- growth rate
	circle.life = circle.life + dt
	if circle.stage == Circles.stages.grow then
		circle.alpha = decreaseExponential(dt, circle.alpha, 0.997)
		if circle.life >= Circles.params.beginWait and circle.cType == Circles.ctypes.playerNote then
			circle.stage = Circles.stages.wait
		end
	elseif circle.stage == Circles.stages.wait then
		circle.alpha = decreaseExponential(dt, circle.alpha, 0.997)
		circle.growth = circle.growth + dt * Circles.params.growthrate
		if circle.life >= Circles.params.endWait then
			circle.stage = Circles.stages.shrink
			circle.growth = Circles.params.shrinkspeed
		end
	elseif circle.stage == Circles.stages.shrink then
		circle.alpha = decreaseExponential(dt, circle.alpha, 0.96)
	elseif circle.stage == Circles.stages.vanish then
		circle.alpha = decreaseExponential(dt, circle.alpha, 0.97)		
	elseif circle.stage == Circles.stages.explode then
		circle.alpha = decreaseExponential(dt, circle.alpha, 0.986)
	end

	if circle.follows then
		if circle.follows.pos then
			circle.pos = {circle.follows.pos[1],circle.follows.pos[2]}
			mapCoords(circle)
		elseif circle.follows.screenPos then
			circle.screenPos = {circle.follows.screenPos[1],circle.follows.screenPos[2]}
		end
	else
		mapCoords(circle)
	end

	-- grow circle
	circle.r = circle.r + circle.growth * dt

	if circle.alpha < 0.01 then
		return false
	end


	return true
end

function Circles.explodeCircle(circle)
	-- replace circle with particle system
	circle.stage = Circles.stages.explode
	circle.growth = Circles.params.explodespeed
	circle.alpha = 1

	Circles.params.currentColor = getNewCircleColor()
	nc = Circles.newCircle(circle.follows, Circles.ctypes.playerNote)
	nc.stage = Circles.stages.explode
	nc.growth = Circles.params.explodetwin
	nc.r = circle.r
	circle.follows = nil
	nc.follows = nil
end

function Circles.pounceStart(entity)
	Circles.params.startSeq = true
	Circles.pounceContinue(entity)
end

function Circles.pounceContinue(entity)
	for i=1,#Circles.list do
		local circ = Circles.list[i] 
		if circ.stage == Circles.stages.wait then
			circ.stage = Circles.stages.grow
			circ.growth = Circles.params.growthspeed
			circ.life = 0
			circ.alpha = 1
			Circles.params.currentColor = getNewCircleColor()
		elseif circ.stage == Circles.stages.grow then
			circ.stage = Circles.stages.vanish
			circ.follows = nil
			circ.growth = Circles.params.vanishspeed
		end
	end

	nc = Circles.newCircle(entity, Circles.ctypes.playerNote)
	Circles.alphaUpdate()

end

function Circles.resetAlpha()
	Circles.params.incAlpha = true
end

function Circles.alphaUpdate()
	-- fade out
	-- nc.alpha = nc.alpha * Circles.params.globalAlpha
	if Circles.params.startSeq then
		Circles.params.globalAlpha = Circles.params.globalAlpha * 0.8
		Circles.params.incAlpha = false
	end
	Circles.params.startSeq = false
end

function Circles.pounceFinish(entity)
	for i=1,#Circles.list do
		local circ = Circles.list[i] 
		if circ.stage == Circles.stages.wait then
			circ.stage = Circles.stages.grow
			circ.growth = Circles.params.growthspeed
			circ.life = 0
			circ.alpha = 1
			Circles.params.currentColor = getNewCircleColor()
		elseif circ.stage == Circles.stages.grow then
			circ.stage = Circles.stages.vanish
			circ.follows = nil
			circ.growth = Circles.params.vanishspeed
		end
	end

	for i = 1,#Circles.list do
		local circ = Circles.list[i] 
		if circ.stage == Circles.stages.grow then
			Circles.explodeCircle(circ)
		end
	end

	nc = Circles.newCircle(entity, Circles.ctypes.playerNote)
	Circles.explodeCircle(nc)

	Circles.alphaUpdate()
end

function Circles.draw()
	local shaderOn = false
	for i=1,#Circles.list do
		local c = Circles.list[i]
		love.graphics.setColor(c.baseColor[1],c.baseColor[2],c.baseColor[3],c.alpha * c.baseAlpha * 255 * Titles.otherOpacity * Circles.params.globalAlpha)
		if c.stage == Circles.stages.explode and Circles.shader then
			Circles.shader:send("rotation",c.angle)
			Circles.shader:send("center",{c.screenPos[1]*VideoModes.scale[1],c.screenPos[2]*VideoModes.scale[2]})
			Circles.shader:send("im2",Circles.shaderImg)
			if not shaderOn then
				love.graphics.setShader(Circles.shader)
				shaderOn = true
			end
		elseif shaderOn and c.stage ~= Circles.stages.explode then
			love.graphics.setShader()
			shaderOn = false
		end
		love.graphics.circle("line",c.screenPos[1],c.screenPos[2],c.r,64)
	end
	love.graphics.setShader()
end
