function math.sign( a )
	if a >= 0 then
		return 1
	else
		return -1
	end
end

function removeOne(t, element)
	for i,v in ipairs(t) do
		if v == element then
			table.remove(t,i)
			return
		end
	end
end

function distWrapSq(posA, posB)
	local dx = math.abs(posA[1] - posB[1])
	dx = math.min(dx, game.worldSize[1]-dx)
	local dy = math.abs(posA[2] - posB[2])
	dy = math.min(dy, game.worldSize[2]-dy)
	return dx*dx + dy*dy
end

function wrapVec(posFrom, posTo)
	local dx = posTo[1] - posFrom[1]
	if math.abs(dx) > game.worldSize[1]*0.5 then
		dx = dx - math.sign(dx) * game.worldSize[1]
	end
	local dy = posTo[2] - posFrom[2]
	if math.abs(dy) > game.worldSize[2]*0.5 then
		dy = dy - math.sign(dx) * game.worldSize[2]
	end

	return dx,dy
end

function getAngle(posA, posB)
	return math.atan2(posB[2] - posA[2], posB[1] - posA[1]) * 180 / math.pi
end

function getOrientation(vec)
	return math.atan2(vec[2], vec[1]) * 180 / math.pi
end

function angleDiff(angleA, angleB)
	local diff = math.abs(angleB%360 - angleA%360)
	return math.min(diff, math.abs(360 - diff))
end

function increaseExponential(dt, var, amount)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	return var
end

function decreaseExponential(dt, var, amount)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	return var
end

function linearApprox(dt, cur, dest, spd)
	local diff = dest - cur
	if math.abs(diff) > 0 then
		local inc = math.sign(diff) * spd * dt
		if math.abs(diff) < math.abs(inc) then
			inc = diff
		end
		cur = cur + inc
	end	
	return cur
end

function hsv2rgb(h, s, v, a)
    if s <= 0 then return v,v,v,a end
    h, s, v = h/360*6, s/255, v/255
    local c = v*s
    local x = (1-math.abs((h%2)-1))*c
    local m,r,g,b = (v-c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end 
	return (r+m)*255,(g+m)*255,(b+m)*255,a
end

function rgb2hsv(r, g, b, a)
	r,g,b = r/255, g/255, b/255
    local min = math.min(r,g,b)
	local max = math.max(r,g,b)

	local v = max
    local delta = max - min;
    if max > 0 then
        s = delta / max
	else 
		s = 0
		h = 0
		return 0,0,v*255,a
	end
	if r >= max then
		h = (g-b)/delta
	elseif g >= max then
		h = 2 + (b-r)/delta
	else
		h = 4 + (r-g)-delta
	end
    h = h * 60
    if h < 0 then
        h = h + 360
	end
    return h,s*255,v*255,a
end
