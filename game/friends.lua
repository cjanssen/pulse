Friends = Friends or {}


function Friends.init()

	Friends.accVel = 50
	Friends.distLimitSq = 600 * 600
	Friends.friction = 0.95
	Friends.overshoot = 3000

	Friends.colors =  {
		base = {300,32,255},
		leaving = {240,32,255},
		annoyed = {359, 32, 255}				
	}

	Friends.triangleimage = getQuad("triangle")

	Friends.list = {}
	
	-- states
	-- 1. wandering
	-- 2. close
	-- 3. engaged
	-- 4. escaping

	Friends.initList()

	Friends.annoyedCount = 0
	Friends.pounceLimit = 3
	Friends.missLimit = 5
	Friends.ignorePlayer = true
	Friends.ignoreTimer = 45
	Friends.ignoreDelay = 15
	Friends.firstPulse = false
	Friends.firstPulseTimer = 3
	Friends.minEchos = 4
	Friends.echoBroadcastBase = 12
	Friends.echoBroadcast = 12

	Friends.imgs = {
		getQuad("big_cell_core_1"),
		getQuad("big_cell_core_2"),
		getQuad("big_cell_core_3")
	}

	Friends.coreimgs = {
		getQuad("core_1"),
		getQuad("core_2"),
		getQuad("core_3")
	}

	Friends.slimeimgs = {
		getQuad("big_cell_slime_1"),
		getQuad("big_cell_slime_2"),
		getQuad("big_cell_slime_3"),
		getQuad("big_cell_slime_4")
	}


	Friends.colorPool = {}

end

function Friends.initList()
	if not game.hasFriends then
		Friends.list = {}
		return
	end

	for i = 1,24 do
		local p = { math.random(game.worldSize[1]), math.random(game.worldSize[2]) }
		local st = 5 --math.random(10) + 5

		Friends.list[i] = {
			pos = { p[1], p[2] },
			screenPos = { p[1], p[2] },
			vel = { 0, 0 },
			acc = { 0, 0 },
			drag = 0.96,
			imgIndex = math.random(3),
			layer = 1,

			state = 1,
			dest = {0,0},
			oldstate = 0,
			resetdest = true,

			--heartRate = 500 + math.random(1500),
			heartRate = player.heartRate,
			strength = st,
			convinced = 0,
			pounceCount = 0,
			annoyedTimer = 0,
			isAnnoyed = false,
			willdraw = true,

			velmul = 1,
			velmulrate = 0.15,

			pulse = {
				timer = 0,
				min = 0.2 + st/40,
				amp = 0.15,
				amount = 0.35 
			},

			rotationSpeed = (math.random()*45 + 15) * math.sign(math.random()-0.5),
			angle = math.random(360),
			
			core = {
				index = math.random(3),
				offset = { math.random(60) - 30, math.random(60) - 30 },
				rotationSpeed = math.random() * 180 - 90,
				angle = math.random(360),
				size = math.random() * 0.3 + 0.25,
			},

			slime = {
				index = math.random(4),
				rotationSpeed = math.random() * 90 - 45,
				angle = math.random(360),
				size = math.random() * 0.4 + 0.4,
			},

			message = {
				idleT = 0
			},

			hook = {
				hooked = false,
				angle = 0,
				currentAngle = 0,
				distToPlayerSq = 1000,
				goalScale = 1,
				currentScale = 1,
				slimeScale = 1,
				rotationDamp = math.random() * 0.35 + 0.4,
				radiusAmp = 0,
				radiusTime = 0,
				radiusSpeed = 50 + math.random() * 80,
			},

			colors = {
				goal  = Friends.colors.base,
				current = Friends.colors.base,
				convinced = false, -- will be overwritten
			},

			notes = {
				pulse = false,
				angry = false,
				beat = false,
			},

			arrow = {
				opacity = 0,	
				screenPos = {0,0},
				angle = 0,
				echotime = 0,
			},

			echo = {
				on = false,
				timer = 0,
				note = false,
			}
		}

	end
end

function Friends.restart()
	Friends.initList()
	Friends.annoyedCount = 0
	Friends.ignorePlayer = true
	Friends.ignoreTimer = 45
	Friends.firstPulse = false
	Friends.firstPulseTimer = 3
	Friends.colorPool = {}
end

function Friends.avoidPlayer()
	for i,v in ipairs(Friends.list) do
		if distWrapSq(player.pos, v.pos) < game.worldSize[1]*game.worldSize[2]/25 then
			v.pos[1] = v.pos[1] + game.worldSize[1]/2
			v.pos[2] = v.pos[2] + game.worldSize[2]/2
		end
	end
end

function Friends.markPosition()
	for i,v in ipairs(Friends.list) do
		v.relativePos = { v.pos[1]-player.pos[1], v.pos[2]-player.pos[2] }
	end
end

function Friends.warp()
	for i,v in ipairs(Friends.list) do
		if v.relativePos then
			v.pos = { player.pos[1] + v.relativePos[1], player.pos[2] + v.relativePos[2] }
		end
	end
end

function Friends.startIgnoring()
	Friends.ignorePlayer = true
	Friends.ignoreTimer = Friends.ignoreDelay
end

function Friends.update( dt )
	if Titles.otherOpacity == 0 then return end

	if not Friends.firstPulse then
		Friends.updateFirstPulse(dt)
	end

	Friends.visibleRadiusSq = game.screenWidth * game.screenHeight
	Friends.computeArrowConstants()
	Friends.updateBroadcast(dt)

	if Friends.ignoreTimer <= 0 then
		Friends.ignorePlayer = false
	elseif Friends.firstPulse then
		Friends.ignoreTimer = Friends.ignoreTimer - dt
	end

	for i,v in ipairs(Friends.list) do
		Friends.updateFriend( v, dt )
	end
end

function Friends.updateBroadcast(dt)
	-- pause when anchored
	if game.mode == 2 and not game.anchorPoint and Friends.firstPulse then
		Friends.echoBroadcast = Friends.echoBroadcast - dt
		if Friends.echoBroadcast <= 0 then
			Friends.echoBroadcast = Friends.echoBroadcastBase
			Friends.echoLocate()
		end
	end
end

function Friends.draw()
	if Titles.otherOpacity == 0 then return end
	for i,v in ipairs(Friends.list) do
		Friends.drawFriend( v )
	end
end

-- Todo: recheck this.
function Friends.updateFirstPulse(dt)
	local playerInBorder = player and player.screenPos and 
		 	(player.screenPos[1] < game.screenWidth*0.3 or player.screenPos[1] > game.screenWidth*0.7 or
		 	player.screenPos[2] < game.screenHeight*0.3 or player.screenPos[2] > game.screenHeight*0.7)

	if playerInBorder and not game.showingHelp and Friends.firstPulseTimer > 0 then
		Friends.firstPulseTimer = Friends.firstPulseTimer - dt
		if Friends.firstPulseTimer <= 0 then
			Titles.callTimer = 5
			game.checkAnchor(player.pos)
		end
	end
end

function Friends.playOwnNote(friend)
	if friend.notes.pulse == false then
		friend.notes.pulse = Sounds.getFriendNote()
	end
	Sounds.playNote(friend.notes.pulse)
end

function Friends.playAngryNote(friend)
	if friend.notes.angry == false then
		friend.notes.angry = Sounds.getAngryNote()
	end
	Sounds.playNote(friend.notes.angry)
end

function Friends.playBeatNote(friend)
	if friend.notes.beat == false then
		friend.notes.beat = Sounds.getBeatNote()
	end
	Sounds.playNote(friend.notes.beat)
end

function Friends.perturbate( friend )
	friend.acc = { friend.acc[1] + (math.random()-0.5) * Friends.accVel, 
					friend.acc[2] + (math.random()-0.5) * Friends.accVel }
		
end


function Friends.slowDown(friend, friction, dt)
	local factor = math.pow(friction, dt * 60)
	if factor <= 1  then
		friend.acc[1] = friend.acc[1] * factor
		friend.acc[2] = friend.acc[2] * factor
	end
end

function Friends.updateFriend( friend, dt )
	local invisible = false
	if friend.state == 1 then -- wander
		local distSq = distWrapSq(player.pos, friend.pos)
		Friends.slowDown(friend, 0.999, dt)
		Friends.perturbate(friend)
		if distSq > Friends.visibleRadiusSq then
			invisible = true
		end

		if Friends.ignorePlayer and distSq < Friends.distLimitSq*16 then
			local dx,dy = wrapVec(player.pos, friend.pos)
			local acmod = 5000/math.sqrt(dx*dx+dy*dy)
			friend.acc[1],friend.acc[2] = dx * acmod, dy * acmod
		end

		if not Friends.ignorePlayer and distSq < Friends.distLimitSq * 0.66 then
			friend.message.idleT = 1200/friend.heartRate
			friend.state = 2
			friend.dest = nil
			Friends.watch(friend)
		end

		if friend.arrow.echotime > 0 then
			friend.arrow.echotime = friend.arrow.echotime - dt
			if friend.arrow.echotime <= 0 then
				Friends.arrowecho(friend)
			end
		end
	elseif friend.state == 2 then -- close
		if game.mode ~= 4 then
			Friends.slowDown(friend, Friends.friction, dt)
			local distSq = distWrapSq(player.pos, friend.pos)
			if distSq > Friends.distLimitSq * 2.44 then
				friend.state = 1
				Friends.unwatch(friend)
			end

			-- "orbit"
			if not friend.dest then
				if game.anchorPoint then
					local randang = math.random() * math.pi * 2
					local randrad = (math.random()*0.5 + 0.5) * math.sqrt(Friends.distLimitSq)
					randrad = randrad * (0.6 + 0.4 * math.abs(math.cos(randang)))
					friend.dest = { 
						(game.anchorPoint[1] + math.cos(randang) * randrad)%game.worldSize[1],
						(game.anchorPoint[2] + math.sin(randang) * randrad)%game.worldSize[2]
					}
				else
					friend.state = 1
					Friends.unwatch(friend)
				end
			else

				-- wrap
				if friend.dest[1] > friend.pos[1] + game.worldSize[1]*0.5 then
					friend.dest[1] = friend.dest[1] - game.worldSize[1]
				elseif friend.dest[1] < friend.pos[1] - game.worldSize[1]*0.5 then
					friend.dest[1] = friend.dest[1] + game.worldSize[1]
				end

				if friend.dest[2] > friend.pos[2] + game.worldSize[2]*0.5 then
					friend.dest[2] = friend.dest[2] - game.worldSize[2]
				elseif friend.dest[2] < friend.pos[2] - game.worldSize[2]*0.5 then
					friend.dest[2] = friend.dest[2] + game.worldSize[2]
				end
			
				friend.acc[1] = (friend.dest[1] - friend.pos[1])*2
				friend.acc[2] = (friend.dest[2] - friend.pos[2])*2
			end

			if friend.echo.on then
				friend.echo.timer = friend.echo.timer - dt
				if friend.echo.timer <= 0 then
					friend.echo.on = false
					Friends.doEcho(friend)
				end
			else
				if not PounceControl.data.isPouncing then
					local m = friend.message
					m.idleT = m.idleT - dt
					if m.idleT <= 0 then
						Friends.resetBeatTimer(friend)
						-- if showing "relax", move but don't beat (cause you could get angry)
						if Titles.helpPhase >= 4 then
							Friends.doBeat(friend)
							if friend.pounceCount ~= 0 then
								-- make it move
								friend.dest = nil
							end
						else
							friend.dest = nil
						end
					end
				else
					Friends.resetBeatTimer(friend)
				end
			end
		end
	elseif friend.state == 3 then -- engaged
		if friend.oldstate ~= 3 then
			friend.hook.hooked = false
			Connectors.newLink(player,friend)
		else
			friend.hook.distToPlayerSq = distWrapSq(player.pos, friend.pos)
			if friend.hook.distToPlayerSq < player.hookDist*player.hookDist then
				if not friend.hook.hooked then
					friend.hook.radiusTime = -90
				end
				friend.hook.hooked = true
				Connectors.removeLink(player,friend)
			end
		end

		Friends.updateHookScale( friend, dt )

		if not friend.hook.hooked then
			friend.velmul = friend.velmul + friend.velmulrate * dt
			local accCap = 1400 * friend.velmul
			if distWrapSq(player.pos, friend.pos) > Friends.distLimitSq then
				accCap = Friends.distLimitSq * friend.velmul
			end


			friend.dest = player.pos

			-- wrap
			if friend.dest[1] > friend.pos[1] + game.worldSize[1]*0.5 then
				friend.dest[1] = friend.dest[1] - game.worldSize[1]
			elseif friend.dest[1] < friend.pos[1] - game.worldSize[1]*0.5 then
				friend.dest[1] = friend.dest[1] + game.worldSize[1]
			end

			if friend.dest[2] > friend.pos[2] + game.worldSize[2]*0.5 then
				friend.dest[2] = friend.dest[2] - game.worldSize[2]
			elseif friend.dest[2] < friend.pos[2] - game.worldSize[2]*0.5 then
				friend.dest[2] = friend.dest[2] + game.worldSize[2]
			end
		
			friend.acc[1] = (friend.dest[1] - friend.pos[1])*2 * friend.velmul
			if math.abs(friend.acc[1]) > accCap then
				friend.acc[1] = math.sign(friend.acc[1]) * accCap
			end

			friend.acc[2] = (friend.dest[2] - friend.pos[2])*2 * friend.velmul
			if math.abs(friend.acc[2]) > accCap then
				friend.acc[2] = math.sign(friend.acc[2]) * accCap
			end
		else
			local angle = math.mod(friend.hook.angle + player.hookAngle + player.angle, 360)

			if angleDiff(angle, friend.hook.currentAngle) > math.max(dt*100,2) then
				friend.hook.currentAngle = friend.hook.currentAngle + player.rotationSpeed * friend.hook.rotationDamp * dt
				angle = friend.hook.currentAngle
			end
			local radius = player.hookDist * (0.75 + 0.25 * friend.hook.radiusAmp)
			friend.pos[1],friend.pos[2] = 
				player.pos[1] + math.cos(angle*math.pi/180) * radius, 
				player.pos[2] + math.sin(angle*math.pi/180) * radius
			friend.acc[1],friend.acc[2] = 0,0
		end

		friend.hook.currentAngle = getAngle(player.pos, friend.pos)

		if friend.echo.on then
			friend.echo.timer = friend.echo.timer - dt
			if friend.echo.timer <= 0 then
				friend.echo.on = false
				Friends.doEcho(friend, friend.echo.volume)
				--consume
				friend.echo.volume = nil
			end
		end

		-- match player beat
		friend.pulse.timer = player.pulse.timer
	elseif friend.state == 4 then -- escaping
		friend.acc[1] = friend.pos[1] - player.pos[1]
		friend.acc[1] = math.sign(friend.acc[1]) * (math.sqrt(Friends.distLimitSq*9) - math.abs(friend.acc[1])) * 0.5
		friend.acc[2] = friend.pos[2] - player.pos[2]
		friend.acc[2] = math.sign(friend.acc[2]) * (math.sqrt(Friends.distLimitSq*9) - math.abs(friend.acc[2])) * 0.5

		Friends.updateHookScale( friend, dt )

		if friend.hook.hooked then
			friend.hook.hooked = false
			Connectors.removeLink(player,friend)
		end

		local distSq = distWrapSq(player.pos, friend.pos)
		if distSq > Friends.distLimitSq * 9 then
			friend.state = 1
		end

	elseif friend.state == 5 then -- annoyed
		friend.annoyedTimer = friend.annoyedTimer + dt
		if friend.annoyedTimer >= 1200/friend.heartRate and friend.isAnnoyed then
			Friends.respond(friend)
		elseif friend.annoyedTimer > 2400/friend.heartRate then
			friend.isAnnoyed = false
			Friends.annoyedCount = Friends.annoyedCount - 1
			if friend.wantsToLeave or #player.followers == 0 then
				friend.wantsToLeave = false
				Friends.unwatch(friend)
				Friends.leave(friend, true)
			else
		 		friend.state = 2
			end
		end
	end

	-- store old state
	friend.oldstate = friend.state

	if not invisible then
		-- regular updates
		updateEntityBeat(friend, dt)
		Friends.updateColors( friend, dt )
		Friends.updateCore( friend, dt )
	end
	updateMove( friend, dt )
	mapCoords( friend )	
	Friends.updateArrow( friend, dt )
end

function Friends.finalPounce()
	local repounce = false

	if not Friends.firstPulse then
		Friends.firstPulse = true
		Friends.ignoreTimer = 0
		Friends.echoLocate(true)
		game.removeAnchor()
	end

	if table.getn(player.watchers) > 0 then
		player.watchers[1].velmul = 0.4
		Friends.join(player.watchers[1])
		Friends.unwatch(player.watchers[1])
		repounce = true
	end
	for i,friend in ipairs(player.followers) do
		Friends.fire(friend)
		if repounce then
			Friends.playTune(friend, i)
		end
	end
end

function Friends.echoLocate(force)
	local arrowEchoCount = 0
	local j = 2
	for i,friend in ipairs(Friends.list) do
		if friend.state == 1 and friend.arrow.opacity > 0 then
			friend.arrow.echotime = j * player.heartPeriod / 2
			arrowEchoCount = arrowEchoCount + 1
			j = j + 1
			Friends.echoBroadcast = Friends.echoBroadcast + player.heartPeriod / 2
		end
	end

	if force and arrowEchoCount < Friends.minEchos then
		Friends.forceEchos(arrowEchoCount)
	end
end


function Friends.arrowecho(friend)
	Circles.newCircle(friend.arrow, Circles.ctypes.strangerReply)
	Friends.playBeatNote(friend)
end


function Friends.forceEchos(countSoFar)
	local friendAngle = math.random(360)
	local randomFriends = {}

	local function inScreen(friend)
		return friend.screenPos[1]>=0 and friend.screenPos[1] <= Options.width and friend.screenPos[2]>=0 and friend.screenPos[2] <= Options.height
	end

	-- get minEchos friends out of screen
	local i = 1
	while #randomFriends < Friends.minEchos - countSoFar do
		local friend = Friends.list[i]
		i = i + 1
		if friend.state == 1 and friend.arrow.opacity == 0 and not inScreen(friend) then
			table.insert(randomFriends, friend)
		elseif friend.state == 1 and friend.arrow.opacity > 0 then
			friendAngle = getAngle(player.pos, friend.pos)
		end
	end

	-- compute angles
	friendAngle = (friendAngle + 360 / Friends.minEchos / 2) % 360
	local angles = {}
	for i=1,Friends.minEchos do
		angles[i] = friendAngle
		friendAngle = (friendAngle + 360 / Friends.minEchos)  % 360
	end

	-- create echos
	local dist = Options.width * 2
	for i,friend in ipairs(randomFriends) do
		local ang = table.remove(angles, math.random(#angles))
		friend.pos[1] = (math.cos(ang * math.pi / 180) * dist + player.pos[1]) % game.worldSize[1]
		friend.pos[2] = (math.sin(ang * math.pi / 180) * dist + player.pos[2]) % game.worldSize[2]
		friend.arrow.echotime = (i+1+countSoFar) * player.heartPeriod / 2
		Friends.echoBroadcast = Friends.echoBroadcast + player.heartPeriod / 2

		mapCoords(friend)
	end
end

function Friends.affectWatchers()
	if table.getn(player.watchers) == 0 then
		return
	end
	-- change color

	if Circles.params.globalAlpha > 0.5 then
		player.watchers[1].convinced = player.watchers[1].convinced + 1
		-- Friends.startEcho(player.watchers[1], note)
	end
end


function Friends.fire(friend)
	Circles.newCircle(friend, Circles.ctypes.friendFire)
	Friends.playOwnNote(friend)
end

function Friends.startEcho(friend, note, volume)
	if not friend.echo.on then
		if friend.heartRate > 100 then
			friend.echo.timer = 600 / friend.heartRate
		else
			friend.echo.timer = 6
		end
		friend.echo.on = true
		friend.echo.note = note
		if volume then
			friend.echo.volume = volume
		else
			friend.echo.volume = nil
		end
	end
end

function Friends.playTune(friend, i)
	friend.echo.on = true
	if friend.notes.pulse == false then
		friend.notes.pulse = Sounds.getFriendNote()
	end
	friend.echo.note = friend.notes.pulse
	friend.echo.timer = player.heartPeriod * i / 2
end

function Friends.doEcho(friend, echovol)
	Circles.newCircle( friend, Circles.ctypes.friendEcho )
	echovol = (echovol or 1) * 0.45
	Sounds.playNoteVolume(friend.echo.note, echovol)
end

function Friends.doBeat(friend)
	if friend.pounceCount == Friends.pounceLimit then
		Friends.annoy(friend)
	else
		Circles.newCircle( friend, Circles.ctypes.strangerReply )
		Friends.playBeatNote(friend)
		friend.pounceCount = friend.pounceCount + 1
	end
end

function Friends.resetBeatTimer(friend)
	if friend.heartRate > 100 then
		friend.message.idleT = 2400 / friend.heartRate
	else
		friend.message.idleT = 24
	end
end

function Friends.annoy(friend)
	friend.pounceCount = 0
	friend.annoyedTimer = 0	
	Friends.resetBeatTimer(friend)

	-- continue pounceing if player has no followers
	if table.getn(player.followers) > 0 then	
		friend.state = 5
		Friends.annoyedCount = Friends.annoyedCount + 1
		friend.isAnnoyed = true
	end
end

function Friends.respond(friend)
	friend.isAnnoyed = false
	Circles.newCircle(friend, Circles.ctypes.angryStranger)
	Friends.playAngryNote(friend)

	-- affect first follower
	if table.getn(player.followers) > 0 then
		player.mistrust = player.mistrust + 1
		if player.mistrust >= Friends.missLimit then
			player.mistrust = 0
			Friends.leave(player.followers[1])
			-- someone leaves! start ignoring
			Friends.startIgnoring()

			for _,watcher in ipairs(player.watchers) do
				watcher.wantsToLeave = true
			end
		end
	end
end

function Friends.join(friend)
	friend.heartRate = player.heartRate
	friend.state = 3
	table.insert(player.followers, friend)
	Friends.recomputeHooks()
	for i,f in ipairs(player.followers) do
		Friends.playOwnNote(f)
	end

	-- when a new friend joins, the other "strangers" swim away
	Friends.startIgnoring()
end

function Friends.checkAttachedFriends()
	if #player.followers < game.friendgoal then return false end
	for i=game.friendgoal,1,-1 do
		if not player.followers[i].hook.hooked then return false end
	end
	return true
end

function Friends.recomputeHooks()
	local n = table.getn(player.followers)
	for i,v in ipairs(player.followers) do
		v.hook.angle = 360 * i / n
	end
end

function Friends.updateHookScale( friend, dt )
	if friend.state == 3 then
		friend.hook.goalScale = 0.70 + 0.30 * math.sqrt(friend.hook.distToPlayerSq / Friends.distLimitSq)
		if friend.hook.goalScale > 1 then
			friend.hook.goalScale = 1
		end
	else
		friend.hook.goalScale = 1
	end

	local diff = friend.hook.goalScale - friend.hook.currentScale
	local inc = math.sign(diff) * 0.5
	if math.abs(inc) > math.abs(diff) then
		inc = diff
	end

	friend.hook.currentScale = friend.hook.currentScale +  inc * dt
	friend.hook.slimeScale = friend.hook.currentScale * friend.hook.currentScale

	-- hook radius
	friend.hook.radiusTime = (friend.hook.radiusTime + friend.hook.radiusSpeed * dt) % 360
	friend.hook.radiusAmp = 1 - math.pow(((math.sin(friend.hook.radiusTime * math.pi / 180)+1)*0.5),3)
end

function Friends.leave(friend, silent)
	friend.state = 4
	local ndx = 0
	for i=1,table.getn(player.followers) do
		if player.followers[i] == friend then
			ndx = i
		end
	end
	if ndx ~= 0 then
		table.remove(player.followers, ndx)
	end
	Friends.recomputeHooks()
	Connectors.removeLink(player, friend)
	friend.message.available = false
	if not silent then
		Sounds.playNote(Sounds.leaveNote)
	end
end

function Friends.watch(friend)
	game.checkAnchor(friend.pos)
	table.insert(player.watchers, friend)
	friend.pounceCount = 0
end

function Friends.unwatch(friend)
	local ndx = 0
	for i=1,table.getn(player.watchers) do
		if player.watchers[i] == friend then
			ndx = i
		end
	end
	if ndx ~= 0 then
		player.watchers.convinced = 0
		friend.pounceCount = 0
		table.remove(player.watchers, ndx)
	end

	if table.getn(player.watchers) == 0 then
		-- for i,v in ipairs(player.followers) do
		-- 	v.mistrust = 0
		-- end
		game.removeAnchor()
	end
end

function Friends.checkMiss()
	-- watchers get annoyed
	for i,v in ipairs(player.watchers) do
		v.convinced = 0
		Friends.annoy(v)
	end
end

function Friends.updateCore( friend, dt )
	friend.core.angle = friend.core.angle + friend.core.rotationSpeed * dt
	if  friend.core.angle > 360 then
		friend.core.angle = friend.core.angle - 360
	elseif friend.core.angle < 0 then
		friend.core.angle = friend.core.angle + 360
	end

	friend.slime.angle = friend.slime.angle + friend.slime.rotationSpeed * dt
	if  friend.slime.angle > 360 then
		friend.slime.angle = friend.slime.angle - 360
	elseif friend.slime.angle < 0 then
		friend.slime.angle = friend.slime.angle + 360
	end
end

function Friends.computeArrowConstants()
	Arrows = {}
	Arrows.border = 12
	Arrows.sc = { Options.width / VideoModes.scale[1], Options.height / VideoModes.scale[2] }
	Arrows.screendist = math.sqrt(Arrows.sc[1]*Arrows.sc[1]+Arrows.sc[2]*Arrows.sc[2]) * 0.5
	Arrows.ringdist = Arrows.screendist * 1.85
	Arrows.longdist = Arrows.screendist * 4.5
	Arrows.screendistSq = Arrows.screendist * Arrows.screendist
	Arrows.ringdistSq = Arrows.ringdist * Arrows.ringdist
	Arrows.longdistSq = Arrows.longdist * Arrows.longdist
	Arrows.sch = {Arrows.sc[1]*0.5,Arrows.sc[2]*0.5}
	Arrows.anglelimit = math.atan2(Arrows.sc[2],Arrows.sc[1])
end

function Friends.updateArrow( friend, dt )
	-- the opacity is a gradient:
	-- if the friend is in the screen, the value is 0
	-- it raises with the distance to a close border
	-- then it fades slowly with the distance

	local dx,dy = wrapVec(game.layerCenters[1], friend.pos)
	local distSq = dx * dx + dy * dy

	-- opacity of the arrow
	if distSq < Arrows.screendistSq then
		friend.arrow.opacity = 0
		friend.willdraw = true
	elseif distSq < Arrows.ringdistSq then
		friend.arrow.opacity = 1 - (Arrows.ringdist - math.sqrt(distSq)) / (Arrows.ringdist - Arrows.screendist)
		friend.willdraw = true
	else
		friend.arrow.opacity = (Arrows.longdist - math.sqrt(distSq)) / (Arrows.longdist - Arrows.ringdist)
		friend.willdraw = true
	end

	if friend.arrow.opacity < 0 then
		friend.arrow.opacity = 0
		willdraw = false
	end

	-- angle of the arrow
	friend.arrow.angle = math.atan2(dy, dx)

	if friend.arrow.angle <= Arrows.anglelimit and friend.arrow.angle > -Arrows.anglelimit then
		-- first quadrant (right)
		friend.arrow.screenPos = {Arrows.sc[1] - Arrows.border, Arrows.sch[2] + Arrows.sch[1] * dy/dx }
		friend.arrow.screenPos[2] = math.max(Arrows.border, math.min( friend.arrow.screenPos[2], Arrows.sc[2]-Arrows.border ) )
	elseif friend.arrow.angle > Arrows.anglelimit and friend.arrow.angle <= math.pi - Arrows.anglelimit then
		-- second quadrant (down)
		friend.arrow.screenPos = {Arrows.sch[1] + Arrows.sch[2] *dx/dy, Arrows.sc[2] - Arrows.border }
		friend.arrow.screenPos[1] = math.max(Arrows.border, math.min( friend.arrow.screenPos[1], Arrows.sc[1]-Arrows.border ) )
	elseif friend.arrow.angle <= -Arrows.anglelimit and friend.arrow.angle > -math.pi + Arrows.anglelimit then
		-- fourth quadrant (up)
		friend.arrow.screenPos = {Arrows.sch[1] - Arrows.sch[2] *dx/dy, Arrows.border }
		friend.arrow.screenPos[1] = math.max(Arrows.border, math.min( friend.arrow.screenPos[1], Arrows.sc[1]-Arrows.border ) )			
	else
		-- third quadrant (left)
		friend.arrow.screenPos = {Arrows.border, Arrows.sch[2] - Arrows.sch[1] * dy/dx}
		friend.arrow.screenPos[2] = math.max(Arrows.border, math.min( friend.arrow.screenPos[2], Arrows.sc[2]-Arrows.border ) )
	end
end

function Friends.getConvincedColor( friend )
	if friend.colors.convinced == false then	
		if table.getn(Friends.colorPool) == 0 then
			-- generate colorpool
			local sortList = {}
			for i=1,14 do
				table.insert(sortList,(i-7)*15 + 120)
			end
			while table.getn(sortList) > 0 do
				local index = math.random(table.getn(sortList))
				table.insert(Friends.colorPool, sortList[index])
				table.remove(sortList, index)
			end
		end

		friend.colors.convinced = { Friends.colorPool[1], 32, 255 }
		table.remove(Friends.colorPool, 1)
	end

	return friend.colors.convinced
end

function Friends.updateColors( friend, dt )
		if friend.state == 1 then -- floating
		friend.colors.current = Friends.colors.base
	elseif friend.state == 2 then -- around
		-- if friend.convinced > 0 then
		-- 	local numiters = table.getn(player.followers) + 2
		-- 	local fraction = 32 * math.max(0,math.min(friend.convinced / numiters,1))
		-- 	local h,s,v,a = rgb2hsv(255-fraction, 223+fraction, 255-fraction, 255)
		-- 	friend.colors.current = {h,s,v}
		-- else
			friend.colors.current = Friends.colors.base
		-- end
	elseif friend.state == 3 then -- following
		friend.colors.current = Friends.getConvincedColor(friend) --friend.colors.convinced
	elseif friend.state == 4 then -- leaving
		friend.colors.current = Friends.colors.leaving
	elseif friend.state == 5 then -- annoyed
		friend.colors.current = Friends.colors.annoyed
	end

end

function Friends.drawFriend( friend )
	local ix,iy,iw,ih
	if friend.willdraw then
		-- color
		local slimeOpacity = 0.5
		Batch:setColor(hsv2rgb(friend.colors.current[1], friend.colors.current[2], friend.colors.current[3], 255*Titles.otherOpacity*slimeOpacity))

		-- slime
		local slimeimage = Friends.slimeimgs[friend.slime.index]
		ix,iy,iw,ih = slimeimage:getViewport()
		Batch:add(slimeimage, 
						friend.screenPos[1],
						friend.screenPos[2],
						(friend.angle + friend.slime.angle) * math.pi / 180,
						friend.slime.size * friend.hook.slimeScale * 2,
						friend.slime.size * friend.hook.slimeScale * 2,
						iw * 0.5 - friend.core.offset[1],
						ih * 0.5 - friend.core.offset[2])

		Batch:setColor(hsv2rgb(friend.colors.current[1], friend.colors.current[2], friend.colors.current[3], 255*Titles.otherOpacity))

		-- center
		local image = Friends.imgs[friend.imgIndex]
		local ofsx, ofsy = 0,0
		local scalex, scaley = 1,1
		if friend.state == 5 then -- trembling when annoyed
			ofsx = math.random() * 6 - 3
			ofsy = math.random() * 6 - 3
			scalex = (math.random()-0.5)*0.3 + 0.8
			scaley = (math.random()-0.5)*0.3 + 0.8
		end
		scalex = scalex * friend.pulse.amount * friend.hook.currentScale
		scaley = scaley * friend.pulse.amount * friend.hook.currentScale
		
		ix,iy,iw,ih = image:getViewport()
		Batch:add(image, 
					friend.screenPos[1] + ofsx, 
					friend.screenPos[2] + ofsy, 
					friend.angle* math.pi / 180, 
					scalex * 2, 
					scaley * 2, 
					iw * 0.5, 
					ih * 0.5)

		-- core
		local coreimage = Friends.coreimgs[friend.core.index]

		local cos = math.cos(friend.angle * math.pi / 180)
		local sin = math.sin(friend.angle * math.pi / 180)
		ofsx = friend.core.offset[1] * cos - friend.core.offset[2] * sin
		ofsy = friend.core.offset[2] * cos + friend.core.offset[1] * sin
		
		local coreScale = ((friend.pulse.amount - friend.pulse.min) * 2 + friend.pulse.min * friend.core.size) * friend.hook.currentScale

		if friend.state == 5 then -- trembling when annoyed
			ofsx = ofsx + math.random() * 6 - 3
			ofsy = ofsy + math.random() * 6 - 3
		end

		ix,iy,iw,ih = coreimage:getViewport()
		Batch:add(coreimage, 
						friend.screenPos[1] + ofsx * scalex,
						friend.screenPos[2] + ofsy * scaley,
						(friend.angle + friend.core.angle) * math.pi / 180,
						coreScale * 2,
						coreScale * 2,
						iw * 0.5 - friend.core.offset[1] * scalex,
						ih * 0.5 - friend.core.offset[2] * scaley)

	end

	-- triangle
	if Friends.firstPulse and friend.arrow.opacity > 0 then
		Batch:setColor(255,255,255, 255 * Titles.otherOpacity * friend.arrow.opacity)
		ix,iy,iw,ih = Friends.triangleimage:getViewport()
		Batch:add(Friends.triangleimage, 
					  	friend.arrow.screenPos[1], 
						friend.arrow.screenPos[2],
						friend.arrow.angle, -- this one in radians
						0.5, 0.5,
						iw * 0.5,
						ih * 0.5)
	end
end
